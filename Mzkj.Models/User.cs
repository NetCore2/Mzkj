﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Mzkj.Models
{
    //[SugarTable("lc_users")]
    public partial class User
    {
        [SugarColumn(IsPrimaryKey =true)]
        public int Id { get; set; }

        [RegularExpression(@"^[a-zA-Z]\w{2,19}$", ErrorMessage = "用户名以字母开头,长度在3到20位之间，只能包含字符、数字和下划线。")]
        //[StringLength(20,ErrorMessage = "用户名必须在6到20位之间",MinimumLength =6)]
        public string Username { get; set; }

        [RegularExpression(@"^\w{6,20}$",ErrorMessage = "密码长度在6-20之间，只能包含字符、数字和下划线。")]
        public string Password { get; set; }

        [RegularExpression(@"^[\u4e00-\u9fa5\w]{1,10}$", ErrorMessage = "输入的昵称非法!")]
        public string Nickname { get; set; }

        [SugarColumn(IsNullable = false, ColumnName = "manager_num")]
        public int? ManagerNum { get; set; } = 0;

        [SugarColumn(ColumnName = "create_time")]//IsOnlyIgnoreInsert 插入数据库默认值
        public long? CreateTime { get; set; } = Common.Tools.SystemHelper.DateTimeToUnixTimestamp(DateTime.Now);

        [SugarColumn(ColumnName ="update_time")]
        public long? UpdateTime { get; set; } = Common.Tools.SystemHelper.DateTimeToUnixTimestamp(DateTime.Now);

        [SugarColumn(IsNullable = false)]
        public int? Status { get; set; } = 1;
        [SugarColumn(IsNullable = false)]
        public int? Delflag { get; set; } = 0;
    }
}
