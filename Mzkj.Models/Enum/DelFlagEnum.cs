﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mzkj.Models.Enum
{
    public enum DelFlagEnum
    {
        /// <summary>
        /// 正常状态
        /// </summary>
        Normal=0,
        /// <summary>
        /// 已经删除状态
        /// </summary>
        Deleted=1
    }
}
