﻿using SqlSugar;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Mzkj.Models
{
    ///<summary>
    ///
    ///</summary>
    public partial class UserInfo
    {
        /// <summary>
        /// Desc:
        /// Default:
        /// Nullable:False
        /// </summary>           
        public int Id { get; set; }

        /// <summary>
        /// Desc:电话
        /// Default:
        /// Nullable:True
        /// </summary>

        
        public string Phone { get; set; }

        /// <summary>
        /// Desc:邮箱
        /// Default:
        /// Nullable:True
        /// </summary>
        [RegularExpression(@"^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+$", ErrorMessage ="邮箱格式不正确!")]
        public string Email { get; set; }

        /// <summary>
        /// Desc:性别
        /// Default:1
        /// Nullable:True
        /// </summary>           
        public byte? Sex { get; set; } = 1;

        /// <summary>
        /// Desc:
        /// Default:
        /// Nullable:True
        /// </summary>
        [SugarColumn(ColumnName = "face_img")]
        public string FaceImg { get; set; }

        /// <summary>
        /// Desc:
        /// Default:
        /// Nullable:True
        /// </summary>
        [SugarColumn(ColumnName = "create_time")]
        public long? CreateTime { get; set; } = Common.Tools.SystemHelper.DateTimeToUnixTimestamp(DateTime.Now);

        /// <summary>
        /// Desc:
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "update_time")]
        public long? UpdateTime { get; set; } = Common.Tools.SystemHelper.DateTimeToUnixTimestamp(DateTime.Now);

        /// <summary>
        /// Desc:
        /// Default:
        /// Nullable:True
        /// </summary>           
        public int Uid { get; set; }

    }
}
