﻿using SqlSugar;
using System;
using System.Linq;
using System.Text;

namespace Mzkj.Models
{
    ///<summary>
    ///
    ///</summary>
    public partial class ActionInfo
    {
        /// <summary>
        /// Desc:
        /// Default:
        /// Nullable:False
        /// </summary>
        [SugarColumn(IsPrimaryKey =true)]
        public int Id {get;set;}

        /// <summary>
        /// Desc:权限名
        /// Default:
        /// Nullable:False
        /// </summary>
        [SugarColumn(IsNullable = false, ColumnName = "action_name")]
        public string ActionName { get; set; }
        public string Dirname { get; set; }

        /// <summary>
        /// Desc:url
        /// Default:
        /// Nullable:False
        /// </summary>

        public string Url { get; set; }

        /// <summary>
        /// Desc:http请求方法
        /// Default:
        /// Nullable:False
        /// </summary>
        
        public string Httpmet { get; set; }

        /// <summary>
        /// Desc:是否是菜单
        /// Default:0
        /// Nullable:False
        /// </summary>
        
        public byte Ismenu { get; set; }

        /// <summary>
        /// Desc:排序
        /// Default:0
        /// Nullable:False
        /// </summary>

        public short? Sort {get;set;}=0;

        /// <summary>
        /// Desc:备注
        /// Default:
        /// Nullable:True
        /// </summary>           
        public string Remark { get; set; }

        /// <summary>
        /// Desc:删除
        /// Default:0
        /// Nullable:False
        /// </summary>           
        public byte? Status { get; set; } = 1;
        public byte? Delflag { get; set; } = 0;

        public int? Pid { get; set; } = 0;

        /// <summary>
        /// Desc:
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "create_time")]
        public long? CreateTime { get; set; } = Common.Tools.SystemHelper.DateTimeToUnixTimestamp(DateTime.Now);

        /// <summary>
        /// Desc:
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "update_time")]
        public long? UpdateTime { get; set; } = Common.Tools.SystemHelper.DateTimeToUnixTimestamp(DateTime.Now);

    }
}
