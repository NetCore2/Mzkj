﻿using System;
using System.Linq;
using System.Text;

namespace Mzkj.Models
{
    ///<summary>
    ///
    ///</summary>
    public partial class UserAction
    {
        /// <summary>
        /// Desc:
        /// Default:
        /// Nullable:False
        /// </summary>           
        public int Id { get; set; }

        /// <summary>
        /// Desc:
        /// Default:
        /// Nullable:False
        /// </summary>           
        public int Uid { get; set; }

        /// <summary>
        /// Desc:
        /// Default:
        /// Nullable:False
        /// </summary>           
        public int Aid { get; set; }
        public short? Permission { get; set; } = 1;

    }
}
