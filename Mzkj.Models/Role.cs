﻿using SqlSugar;
using System;
using System.Linq;
using System.Text;

namespace Mzkj.Models
{
    ///<summary>
    ///
    ///</summary>
    public partial class Role
    {
        public Role()
        {


        }
        /// <summary>
        /// Desc:
        /// Default:
        /// Nullable:False
        /// </summary>           
        public int Id { get; set; }

        /// <summary>
        /// Desc:角色名
        /// Default:
        /// Nullable:False
        /// </summary>
        [SugarColumn(IsNullable = false, ColumnName = "role_name")]
        public string RoleName { get; set; }

        /// <summary>
        /// Desc:备注
        /// Default:
        /// Nullable:True
        /// </summary>           
        public string Remark { get; set; }
        public byte? Status { get; set; } = 1;
        /// <summary>
        /// Desc:
        /// Default:0
        /// Nullable:False
        /// </summary>           
        public byte Delflag { get; set; } = 0;

        /// <summary>
        /// Desc:
        /// Default:
        /// Nullable:True
        /// </summary>
        [SugarColumn(ColumnName = "create_time")]
        public long? CreateTime { get; set; }

        /// <summary>
        /// Desc:
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "update_time")]
        public long? UpdateTime { get; set; }

    }
}
