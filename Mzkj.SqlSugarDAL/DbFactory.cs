﻿using Mzkj.Common;
using SqlSugar;
using System;

namespace Mzkj.SqlSugarDAL
{
    public class DbFactory
    {
       
        public static SqlSugarClient SqlClient() {
            //return new SqlSugarClient(Config.SqlSugarConfig);
            var db = new SqlSugarClient(Config.SqlSugarConfig);
            db.MappingTables.AddRange(Config.MpList);
            db.MappingColumns.AddRange(Config.MpColumnList);
            return db;
        }
        public static PageModel Page()
        {
            return Config.pageCommon;
        }
    }
}
