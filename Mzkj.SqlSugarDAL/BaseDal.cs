﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Mzkj.SqlSugarDAL
{
    public class BaseDal<T> where T:class,new()
    {

        public string orderField = "Id Asc";//默认排序"Id Asc"
        public SqlSugarClient Db
        {
            get { return DbFactory.SqlClient(); }
        }
        public PageModel Pages
        {
            get { return DbFactory.Page(); }
        }
        
        #region 简单查询
        /// <summary>
        /// 根据主键查询
        /// </summary>
        /// <returns>返回单个Model对象</returns>
        public T Find(dynamic id)
        {
            return Db.Queryable<T>().InSingle(id);
        }
        public List<T> Find(dynamic[] ids)
        {
            return Db.Queryable<T>().In(ids).ToList(); ;
        }

        /// <summary>
        /// 查询第一个
        /// </summary>
        /// <returns>返回单个Model对象</returns>
        public T First()
        {
            return First();
        }
        //public T First(string orderField)
        //{
        //    return Db.Queryable<T>().OrderBy(orderField).First();
        //}

        /// <summary>
        /// 带Lambda查询第一个
        /// </summary>
        /// <param name="whereLambda">拉姆达表达式</param>
        /// <returns></returns>
        public T First(Expression<Func<T, bool>> whereLambda)
        {
            return First(whereLambda, orderField);
        }
        public T First(Expression<Func<T, bool>> whereLambda, string orderField)
        {
            return Db.Queryable<T>().Where(whereLambda).OrderBy(orderField).First();
        }
        #endregion

        #region 查询
        #region 查询返回全部List数据 带排序
        /// <summary>
        /// 查询返回全部List数据
        /// </summary>
        /// <param name="orderField">排序字符串(默认:"Id Asc")</param>
        /// <returns>返回List集合</returns>
        public List<T> GetList()
        {
            var getAll = Db.Queryable<T>().OrderBy(orderField).ToList();
            return getAll;
        }
        //public List<T> GetList(string orderField)
        //{
        //    var getAll = Db.Queryable<T>().OrderBy(orderField).ToList();
        //    return getAll;
        //}
        #endregion

        #region 自定义条件查询返回List对象 带排序
        /// <summary>
        /// 自定义条件查询返回List对象
        /// </summary>
        /// <param name="whereLambda">拉姆达查询语句如(it=>it.Id>1)</param>
        /// <param name="orderField">排序字符串(默认:"Id Asc")</param>
        /// <returns>ISugarQueryable</returns>
        public List<T> GetList(Expression<Func<T, bool>> whereLambda)
        {
            return GetList(whereLambda, orderField);
        }
        public List<T> GetList(Expression<Func<T, bool>> whereLambda, string orderField)
        {
            return Db.Queryable<T>().Where(whereLambda).OrderBy(orderField).ToList();
        }
        #endregion

        #region 自定义分页查询返回List 带排序
        /// <summary>
        /// 自定义条件查询数据
        /// </summary>
        /// <param name="pageIndex">当前页码数</param>
        /// <param name="page">页码PageModel</param>
        /// <param name="whereLambda">拉姆达查询语句如(it=>id.Id>1)</param>
        /// <returns>返回List集合</returns>
        public List<T> GetPageList(Expression<Func<T, bool>> whereLambda, int pageIndex = 1)
        {
            return GetPageList(whereLambda, pageIndex, orderField, Pages);
        }

        public List<T> GetPageList(Expression<Func<T, bool>> whereLambda, int pageIndex, string orderField)
        {
            return GetPageList(whereLambda, pageIndex, orderField, Pages);
        }
        public List<T> GetPageList(Expression<Func<T, bool>> whereLambda, int pageIndex, string orderField, PageModel page)
        {
            page.PageIndex = pageIndex;
            int count = 0;
            var result = Db.Queryable<T>().Where(whereLambda).OrderBy(orderField).ToPageList(page.PageIndex, page.PageSize, ref count);
            page.PageCount = count;
            return result;
        }
        #endregion

        #region 复杂的排序查询
        /// <summary>
        /// 查询返回全部ISugarQueryable对象
        /// </summary>
        /// <returns>ISugarQueryable</returns>
        public ISugarQueryable<T> GetDatas()
        {
            var getAll = Db.Queryable<T>();
            return getAll;
        }
        /// <summary>
        /// 自定义条件查询返回ISugarQueryable对象
        /// </summary>
        /// <param name="whereLambda">拉姆达查询语句如(it=>id.Id>1)</param>
        /// <returns>ISugarQueryable</returns>
        public ISugarQueryable<T> GetDatas(Expression<Func<T, bool>> whereLambda)
        {
            return Db.Queryable<T>().Where(whereLambda);
        }


        /*public ISugarQueryable<T> GetPageDatas(int pageIndex = 1)
        {
            return GetPageDatas(pageIndex, orderField, Pages);
        }*/
        public ISugarQueryable<T> GetPageDatas(int pageIndex = 1)
        {
            return GetPageDatas(pageIndex, orderField, Pages);
        }
        public ISugarQueryable<T> GetPageDatas(int pageIndex, string orderField)
        {
            return GetPageDatas(pageIndex, orderField, Pages);
        }
        public ISugarQueryable<T> GetPageDatas(int pageIndex, string orderField, PageModel page)
        {
            page.PageIndex = pageIndex;
            var Tempdb = Db.Queryable<T>();
            var result = Tempdb.OrderBy(orderField).Take(page.PageSize).Skip((page.PageIndex - 1) * page.PageSize);
            Pages.PageCount = Tempdb.Count();
            return result;
        }

        /// <summary>
        /// 自定义条件查询数据
        /// </summary>
        /// <param name="pageIndex">当前页码数</param>
        /// <param name="page">页码PageModel</param>
        /// <param name="whereLambda">拉姆达查询语句如(it=>id.Id>1)</param>
        /// <returns>返回List集合</returns>
        public ISugarQueryable<T> GetPageDatas(Expression<Func<T, bool>> whereLambda, int pageIndex = 1)
        {
            return GetPageDatas(whereLambda, pageIndex, orderField, Pages);
        }
        public ISugarQueryable<T> GetPageDatas(Expression<Func<T, bool>> whereLambda, int pageIndex, string orderField)
        {
            return GetPageDatas(whereLambda, pageIndex, orderField, Pages);
        }
        public ISugarQueryable<T> GetPageDatas(Expression<Func<T, bool>> whereLambda, int pageIndex, string orderField, PageModel page)
        {
            page.PageIndex = pageIndex;
            var Tempdb = Db.Queryable<T>();
            var result = Tempdb.Where(whereLambda).OrderBy(orderField).Take(page.PageSize).Skip((page.PageIndex - 1) * page.PageSize);
            Pages.PageCount = Tempdb.Count();
            return result;
        }
        #endregion

        #region 查询是否存在数据 IsAny
        public bool IsAny(Expression<Func<T, bool>> whereLambda)
        {
            return Db.Queryable<T>().Any(whereLambda);
        }
        #endregion

        #endregion

        #region 插入
        /// <summary>
        /// 返回受影响的行数
        /// </summary>
        /// <param name="inObject">List,Dictionary,T对象,T[]数组</param>
        /// <returns></returns>
        public IInsertable<T> Insert(dynamic inObject)
        {
            return Db.Insertable<T>(inObject);
        }
        public IInsertable<T> Insert(dynamic inObject, Expression<Func<T, T>> fieldLambda)
        {
            return Db.Insertable<T>(inObject).IgnoreColumns(fieldLambda);
        }
        /// <summary>
        /// 返回布尔值,更新操作 主键必须有值
        /// </summary>
        /// <param name="inObject">List,Dictionary,T对象,T[]数组</param>
        /// <returns></returns>
        public bool Add(dynamic inObject)
        {
            return Db.Insertable<T>(inObject).ExecuteCommand() > 0;
        }
        #endregion

        #region 修改
        /// <summary>
        /// 返回布尔值,更新操作 主键必须有值
        /// </summary>
        /// <param name="inObject">List,Dictionary,T对象,T[]数组</param>
        /// <returns></returns>
        public bool Save(dynamic updateObj)
        {
            return Db.Updateable<T>(updateObj).ExecuteCommand() > 0;
        }
        /// <summary>
        /// 返回布尔值,更新操作 主键必须有值
        /// </summary>
        /// <param name="inObject">T对象</param>
        /// <returns></returns>
        public bool Save(T updateObj,bool field)
        {
            /*
            Type t = updateObj.GetType();
            List<string> list = new List<string>();
            if (field) { 
                foreach (System.Reflection.PropertyInfo s in t.GetProperties()) //循环遍历  
                {
                    if (s.GetValue(updateObj, null)==null || string.IsNullOrWhiteSpace(s.GetValue(updateObj, null).ToString()))
                    {
                        list.Add(s.Name);
                    }
                }
            }
            return Db.Updateable(updateObj).IgnoreColumns(it => list.Contains(it)).ExecuteCommand()>0;
            */
            return Db.Updateable(updateObj).Where(field).ExecuteCommand() > 0;
        }

        /// <summary>
        /// 主键必须有值
        /// </summary>
        /// <param name="updateObj">List,Dictionary,T对象,T[]数组</param>
        /// <returns></returns>
        public IUpdateable<T> Update(dynamic updateObj)
        {
            return Db.Updateable<T>(updateObj);
        }

        /// <summary>
        /// 常用更新的模式
        /// </summary>
        /// <param name="columns">需要更新的拉姆达表达式(如:it => new Student() { Name = "a", CreateTime = DateTime.Now })</param>
        /// <param name="whereExpression">拉姆达表达式(如:it => it.Id == 11)按照该条件更新</param>
        /// <returns></returns>
        public bool Update(Expression<Func<T, bool>> columns, Expression<Func<T, bool>> whereLambda)
        {
            return Db.Updateable<T>().UpdateColumns(columns).Where(whereLambda).ExecuteCommand() > 0;
        }
        #endregion

        #region 删除
        public bool Delete(T deleteObj)
        {
            return Db.Deleteable<T>().Where(deleteObj).ExecuteCommand() > 0;
        }
        public bool Delete(Expression<Func<T, bool>> whereLambda)
        {
            return Db.Deleteable<T>().Where(whereLambda).ExecuteCommand() > 0;
        }
        public bool Del(dynamic id)
        {
            return Db.Deleteable<T>().In(id).ExecuteCommand() > 0;
        }
        public bool Del(dynamic[] ids)
        {
            return Db.Deleteable<T>().In(ids).ExecuteCommand() > 0;
        }
        #endregion

        #region 聚合查询
        /// <summary>
        /// 返回数据总数
        /// </summary>
        /// <returns>整数</returns>
        public int Count()
        {
            return Db.Queryable<T>().Count();
        }
        /// <summary>
        /// 返回数据总数
        /// </summary>
        /// <param name="whereLambda">拉姆达表达式where</param>
        /// <returns>整数</returns>
        public int Count(Expression<Func<T, bool>> whereLambda)
        {
            return Db.Queryable<T>().Where(whereLambda).Count();
        }
        #endregion

        public IAdo Ado()
        {
            return Db.Ado;
        }

        #region 自定义查询
        /// <summary>
        /// 自定义查询
        /// </summary>
        /// <param name="u"></param>
        /// <param name="dic"></param>
        /// <returns></returns>
        public ISugarQueryable<T> Search(ISugarQueryable<T> u, Dictionary<string, string> dic) {
            List<string> listKey1 = new List<string>();
            List<string> listKey2 = new List<string>();
            foreach (KeyValuePair<string, string> item in dic)
            {
                string[] str = item.Key.Split("__");
                listKey1.Add(str[0]);
                listKey2.Add(str[1]);
            }
            for (int i = 0; i < listKey2.Count; i++)
            {
                dic.TryGetValue(listKey1[i] + "__" + listKey2[i], out string value);
                switch (listKey1[i])
                {
                    case "like":
                        u = u.Where(listKey2[i] + " like '%" + value + "%'");
                        break;
                    case "eq":
                        u = u.Where(listKey2[i] + " = '" + value + "'");
                        break;
                    case "gt":
                        u = u.Where(listKey2[i] + " > '" + value + "'");
                        break;
                    case "egt":
                        u = u.Where(listKey2[i] + " >= '" + value + "'");
                        break;
                    case "lt":
                        u = u.Where(listKey2[i] + " < '" + value + "'");
                        break;
                    case "elt":
                        u = u.Where(listKey2[i] + " <= '" + value + "'");
                        break;
                    case "between":
                        u = u.Where(listKey2[i] + " between '" + value.Split("__")[0] + "' and '" + value.Split("__")[1] + "'");
                        break;
                    default:
                        break;
                }
            }
            return u;
        }

        public List<T> Search(ISugarQueryable<T> u,int pageIndex, string orderField, PageModel page)
        {
            page.PageIndex = pageIndex;
            int count = 0;
            var result = u.OrderBy(orderField).ToPageList(page.PageIndex, page.PageSize, ref count);
            page.PageCount = count;
            return result;
        }
        #endregion

        //多表查询 预留
    }
}
