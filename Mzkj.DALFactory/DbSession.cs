﻿using Mzkj.IDal;
using Mzkj.SqlSugarDAL;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mzkj.DALFactory
{
    public class DbSession : IDbSession
    {
        public IUserDal UserDal => StaticDalFactory.GetUserDal();
        public IUserInfoDal UserInfoDal => StaticDalFactory.GetUserInfoDal();

        public IActionInfoDal ActionInfoDal => StaticDalFactory.GetActionInfoDal();
        public IRoleDal RoleDal => StaticDalFactory.GetRoleDal();
        public IUserRoleDal UserRoleDal => StaticDalFactory.GetUserRoleDal();
        public IRoleActionDal RoleActionDal => StaticDalFactory.GetRoleActionDal();
    }
}
