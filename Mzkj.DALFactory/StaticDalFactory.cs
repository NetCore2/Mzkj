﻿using Mzkj.IDal;
using Mzkj.SqlSugarDAL;
using System;

namespace Mzkj.DALFactory
{
    public class StaticDalFactory
    {
        //可以封装配置文件,随时改变ORM
        public static IUserDal GetUserDal()
        {
            IUserDal user= new UserDal();
            return user;
        }
        //可以封装配置文件,随时改变ORM
        public static IUserInfoDal GetUserInfoDal()
        {
            IUserInfoDal userInfo = new UserInfoDal();
            return userInfo;
        }
        /// <summary>
        /// 权限Dal
        /// </summary>
        /// <returns></returns>
        public static IActionInfoDal GetActionInfoDal()
        {
            IActionInfoDal actionInfo = new ActionInfoDal();
            return actionInfo;
        }
        /// <summary>
        /// 角色Dal
        /// </summary>
        /// <returns></returns>
        public static IRoleDal GetRoleDal()
        {
            IRoleDal role = new RoleDal();
            return role;
        }
        /// <summary>
        /// 用户关联角色Dal
        /// </summary>
        /// <returns></returns>
        public static IUserRoleDal GetUserRoleDal()
        {
            IUserRoleDal userRole = new UserRoleDal();
            return userRole;
        }
        /// <summary>
        /// 角色关联权限Dal
        /// </summary>
        /// <returns></returns>
        public static IRoleActionDal GetRoleActionDal()
        {
            IRoleActionDal roleAction = new RoleActionDal();
            return roleAction;
        }
    }
}
