﻿using Mzkj.IDal;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mzkj.DALFactory
{
    public class DbSessionFactory
    {
        private static IDbSession db;
        static readonly object Padlock = new object();
        public static IDbSession GetDbSession()
        {
            if (db == null)
            {
                lock (Padlock)
                {
                    if (db == null)
                    {
                        db = new DbSession();
                    }
                }
            }
            return db;
        }
    }
}
