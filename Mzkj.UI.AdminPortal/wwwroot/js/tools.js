﻿var luck = {};
//时间戳过滤开始
luck.d_time = function (value, row, index) {
    if (value < 1) {
        return '无';
    } else {
        var d = new Date(value * 1000);
        return luck.formatDate(d, '');
    }
};
luck.d_date = function (value, row, index) {
    if (value < 1) {
        return '无';
    } else {
        var d = new Date(value * 1000);
        return luck.formatDate(d, 'm');
    }
};
luck.formatDate = function (now, d) {
    var year = now.getFullYear();
    var month = now.getMonth() + 1;
    var date = now.getDate();
    var hour = now.getHours();
    var minute = now.getMinutes();
    var second = now.getSeconds();
    if (month < 10) {
        month = '0' + month;
    }
    if (date < 10) {
        date = '0' + date;
    }
    if (hour < 10) {
        hour = '0' + hour;
    }
    if (minute < 10) {
        minute = '0' + minute;
    }
    if (second < 10) {
        second = '0' + second;
    }
    if (d === 'm') {
        return year + "-" + month + "-" + date;
    } else {
        return year + "-" + month + "-" + date + " " + hour + ":" + minute + ":" + second;
    }
};
//结束时间戳
