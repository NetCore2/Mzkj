/*
 * @Author: Steven
 * @Date:   2018-04-19
 * +----------------------------------------------------------------------
 * | Luck [  ]
 * | Copyright (c) 2018  All rights reserved.
 * | Licensed (  )
 * | Author: Vashi <289650682@qq.com>
 * +----------------------------------------------------------------------
 */

layui.define(['jquery','table'], function(exports) {
    var $ = layui.jquery;
    var ajax_options = {
        method: "get",
        dataType: "json",
        url: "",
        data: {},
        timeout: 5000,
        cache: false,
        loading:true
    };
    var hebing=function (jsonbject1, jsonbject2) {
        var resultJsonObject = {};
        for (var atts in jsonbject1) {
            resultJsonObject[atts] = jsonbject1[atts];
        }
        for (var att in jsonbject2) {
            resultJsonObject[att] = jsonbject2[att];
        }
        return resultJsonObject;
    };
    exports('ajax', function (options, callback) {
        var newOp = hebing(ajax_options, options);
        var l;
        $.ajax({
            type: newOp.method,
            url: newOp.url,
            dataType: newOp.dataType,
            data: newOp.data,
            timeout: newOp.timeout,
            cache: newOp.cache,
            beforeSend: function () {
                if (newOp.loading === true) {
                    l = layer.load(1);
                }
            },
            error: function (XMLHttpRequest, status, thrownError) {
                layer.msg('网络繁忙，请稍后重试...', { icon: 2 });
            },
            success: function (data) {
                if (!callback) {
                    layer.msg(data.msg, { icon: data.code });
                } else {
                    callback(data, newOp);
                }
                return;
            },
            complete: function () {
                if (newOp.loading === true) {
                    layer.close(l);
                }
            }
        });
    });
});