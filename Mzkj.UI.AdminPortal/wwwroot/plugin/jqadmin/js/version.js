layui.config({
    base: '../plugin/jqadmin/js/',
    version: "2.0.1-simple"
}).extend({
    jqelem: 'jqmodules/jqelem',
    jqmenu: 'jqmodules/jqmenu',
    tabmenu: 'jqmodules/tabmenu',
    jqbind: 'jqmodules/jqbind',
    jqtags: 'jqmodules/jqtags',
    jqform: 'jqmodules/jqform',
    echarts: 'lib/echarts',
    treeGrid: 'layui/lay/modules/treeGrid',
    webuploader: 'lib/webuploader',
    jqcitys: "jqmodules/jqcitys"
});