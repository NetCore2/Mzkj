﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mzkj.UI.AdminPortal.Filters
{
    public class HttpExceptionFilter :IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            //错误信息写到日志文件
            Common.Logs.LogHelper.WriteLog(context.Exception.ToString());
        }
    }
}
