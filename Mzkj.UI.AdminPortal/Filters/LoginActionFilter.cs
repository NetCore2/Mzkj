﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Mzkj.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mzkj.UI.AdminPortal.Filters
{
    public class LoginActionFilter : ActionFilterAttribute
    {
        public bool IsCheckLogin { get; set; }


        public override void OnActionExecuted(ActionExecutedContext context)
        {
            base.OnActionExecuted(context);
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            base.OnActionExecuting(context);

            if (IsCheckLogin)
            {
                //使用Cookies+redis
                context.HttpContext.Request.Cookies.TryGetValue("userLoginId", out string userGuid);
                if (userGuid == null)
                {
                    context.HttpContext.Response.Redirect("/Admin/Login");
                    return;
                }

                string user = Common.Cache.CacheHelper.GetCache(userGuid);
                if (user == null)
                {
                    //用户长时间不操作超时
                    context.HttpContext.Response.Redirect("/Admin/Login");
                    return;
                }
                User userInfo = Common.Tools.StringHelper.ParseFormJson<User>(user);
                //滑动窗口机制
                Common.Cache.CacheHelper.SetCache(userGuid, user, Common.Config.minutes);

            }
        }
    }
}
