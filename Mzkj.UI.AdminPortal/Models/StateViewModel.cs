﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mzkj.UI.AdminPortal.Models
{
    public class StateViewModel<T>// where T:class,new()
    {
        /// <summary>
        /// Code:
        /// 1   正常状态
        /// 2   错误状态
        /// 5   网络错误
        /// </summary>
        public int Code { set; get; }
        public string Msg { set; get; }
        public T Data { set; get; }
        public int Count { set; get; }
    }
}
