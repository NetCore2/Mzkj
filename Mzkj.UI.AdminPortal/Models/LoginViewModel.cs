﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Mzkj.UI.AdminPortal.Models
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "用户名不能为空。")]
        public string Username { get; set; }

        public string Password { get; set; }
        public string Code { get; set; }
    }
}
