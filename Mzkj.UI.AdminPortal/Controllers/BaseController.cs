﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Mzkj.BLL;
using Mzkj.Common.Tools;
using Mzkj.IBLL;
using Mzkj.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mzkj.UI.AdminPortal.Controllers
{
    public abstract class BaseController:Controller
    {
        public bool IsCheckLogin = true;

        public User LoginUser { get; set; }
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            base.OnActionExecuting(context);
            if (IsCheckLogin)
            {
                //使用Cookies+redis
                Request.Cookies.TryGetValue("userLoginId", out string userGuid);
                if (userGuid == null) {
                    context.HttpContext.Response.Redirect("/Admin/Login");
                    return;
                }
                string user=Common.Cache.CacheHelper.GetCache(userGuid);
                if (user == null)
                {
                    //用户长时间不操作超时
                    context.HttpContext.Response.Redirect("/Admin/Login");
                    return;
                }
                User userInfo = StringHelper.ParseFormJson<User>(user);
                LoginUser = userInfo;
                //滑动窗口机制
                Common.Cache.CacheHelper.SetCache(userGuid, user, Common.Config.minutes);


                //校验权限
                if (userInfo.Username == "admin")
                {
                    return;
                }

                //权限的连接
                string url = context.HttpContext.Request.Path;
                string httpMethod = context.HttpContext.Request.Method.ToLower();

                IActionInfoService actionInfo = new ActionInfoService();
                var ac=actionInfo.GetDatas().Where(it => it.Url.ToLower() == url.ToLower() && it.Httpmet.ToLower() == httpMethod).First();

                if (ac == null)
                {
                    Response.Redirect("/Error.html");
                }

                IUserRoleService userRole = new UserRoleService();
                IRoleActionService roleAction = new RoleActionService();
                //查询登录用户的角色
                List<UserRole> luserRole = userRole.GetDatas().Where(it => it.Uid == userInfo.Id).ToList();
                List<int> rids = new List<int>();
                for (int i = 0; i < luserRole.Count; i++)
                {
                    rids.Add(luserRole[i].Rid);
                }

                //根据角色查询权限Id
                List<RoleAction> lroleAction = roleAction.GetDatas().Where(it => rids.Contains(it.Rid)).ToList();
                List<int> aids = new List<int>();
                for (int i = 0; i < lroleAction.Count; i++)
                {
                    aids.Add(lroleAction[i].Aid);
                }
                //根据权限ID查询权限
                List<ActionInfo> a = actionInfo.GetDatas().Where(it => aids.Contains(it.Id)).ToList();
                if (a.Count <= 0)
                {
                    Response.Redirect("/Error.html");
                }
            }
        }
    }
}
