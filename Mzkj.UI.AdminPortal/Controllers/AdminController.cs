﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Mzkj.BLL;
using Mzkj.Common;
using Mzkj.Common.Cache;
using Mzkj.Common.Tools;
using Mzkj.IBLL;
using Mzkj.Models;
using Mzkj.UI.AdminPortal.Filters;
using Mzkj.UI.AdminPortal.Models;

namespace Mzkj.UI.AdminPortal.Controllers
{
    public class AdminController : Controller
    {
        [LoginActionFilter(IsCheckLogin = true)]
        public IActionResult Index()
        {
            string userGuid = CookiesHelper.Get("userLoginId", HttpContext);
            if (!string.IsNullOrEmpty(userGuid))
            {
                string userStr = CacheHelper.GetCache(userGuid);
                if (!string.IsNullOrEmpty(userStr)) {
                    User user = StringHelper.ParseFormJson<User>(userStr);
                    ViewData["uname"] = user.Username;
                }
            }
            return View();
        }

        #region 控制台首页
        [LoginActionFilter(IsCheckLogin = true)]
        public IActionResult Welcome()
        {
            return Content("Welcome");
        }
        #endregion

        #region 登录
        [HttpGet]
        public IActionResult Login()
        {
            //登录之后禁止进入登录页面
            string userGuid = CookiesHelper.Get("userLoginId", HttpContext);
            if (userGuid != null) {
                string user = CacheHelper.GetCache(userGuid);
                if (user != null) {
                    return RedirectToAction("Index");
                }
            }
            return View();
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]//csrf防伪
        public IActionResult Login(LoginViewModel model) {
            //return Content(StringHelper.GetJson<LoginViewModel>(model));
            StateViewModel<StateUrlViewModel> state = new StateViewModel<StateUrlViewModel>() { Code=5,Msg= "网络错误" };
            if (ModelState.IsValid)
            {
                //添加错误状态 2
                state.Code = 2;

                string code = HttpContext.Session.GetString("LoginCode");
                //验证成功清除验证嘛
                HttpContext.Session.Remove("LoginCode");

                if (string.IsNullOrEmpty(code))
                {
                    state.Msg = "验证码错误。";
                }
                //为了只使用一个return 增加else
                else {
                    if (model.Code.ToLower() == code.ToLower())//不区分大小写
                    {

                        //和数据库对比
                        IUserService userService = new UserService();
                        var user = userService.First(
                            it => it.Username == model.Username && it.Password == CryptoHelper.EncryptText(model.Password) && it.Delflag==0 && it.ManagerNum==1
                            );
                        if (user != null)
                        {
                            //登录成功记录到Redis+Cookies
                            string userLoginId = Guid.NewGuid().ToString();
                            CacheHelper.AddCache(userLoginId, StringHelper.GetJson<User>(user), Config.minutes);
                            CookiesHelper.Add("userLoginId", userLoginId,HttpContext);

                            state.Code = 1;
                            state.Msg = "登录成功,正在跳转到系统首页!";
                            state.Data=new StateUrlViewModel() { Url="/" };
                            //跳转到系统首页
                            //return RedirectToAction("Index");
                        }
                        //没有查询到数据
                        state.Msg = "用户名或密码错误。";
                    }
                    else {
                        state.Msg = "验证码错误。";
                    }
                }
            }
            else { 
                state.Msg = "用户名或密码错误。";
                state.Code = 2;
            }
            return new JsonResult(state);
        }
        #endregion

        #region 退出登录
        /// <summary>
        /// 退出登录!
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult LoginOut()
        {
            string userGuid = CookiesHelper.Get("userLoginId", HttpContext);
            //删除redis key
            CacheHelper.ClearCache(userGuid);
            //HttpContext.Request.Cookies.TryGetValue("userLoginId", out string user2Guid);
            CookiesHelper.Remove("userLoginId", HttpContext);
            return RedirectToAction("Login");
        }
        #endregion

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult LoadUserMenu()
        {
            string userGuid = CookiesHelper.Get("userLoginId", HttpContext);
            if (userGuid != null)
            {
                string userStr = CacheHelper.GetCache(userGuid);
                if (!string.IsNullOrEmpty(userStr))
                {
                    User user = StringHelper.ParseFormJson<User>(userStr);

                    IUserRoleService userRole = new UserRoleService();
                    IRoleActionService roleAction = new RoleActionService();
                    //IUserActionService userAction = newUserActionService();
                    IActionInfoService actionInfo = new ActionInfoService();

                    List<UserRole> luserRole=userRole.GetDatas().Where(it => it.Uid == user.Id).ToList();
                    List<int> rids = new List<int>();
                    for (int i = 0; i < luserRole.Count; i++)
                    {
                        rids.Add(luserRole[i].Rid);
                    }

                    List<RoleAction> lroleAction = roleAction.GetDatas().Where(it => rids.Contains(it.Rid)).ToList();
                    List<int> aids = new List<int>();
                    for (int i = 0; i < lroleAction.Count; i++)
                    {
                        aids.Add(lroleAction[i].Aid);
                    }

                    List<ActionInfo> a=actionInfo.GetDatas().Where(it => aids.Contains(it.Id) && it.Ismenu==1).ToList();
                    return new JsonResult(a);
                }
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }
    }
}
