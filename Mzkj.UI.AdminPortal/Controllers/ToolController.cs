﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Mzkj.Common.Tools;

namespace Mzkj.UI.AdminPortal.Controllers
{
    public class ToolController : Controller
    {
        /// <summary>
        /// 验证码
        /// </summary>
        /// <returns></returns>
        public IActionResult Vcode()
        {
            string code = "";
            System.IO.MemoryStream ms = (new ValidateCodeTools()).Create(out code);
            HttpContext.Session.SetString("LoginCode", code);
            Response.Body.Dispose();
            return File(ms.ToArray(), @"image/png");
        }
    }
}