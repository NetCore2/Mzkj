﻿using Microsoft.AspNetCore.Mvc;
using Mzkj.BLL;
using Mzkj.IBLL;
using Mzkj.Models;
using Mzkj.UI.AdminPortal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mzkj.UI.AdminPortal.Controllers
{
    public class TreeController
    {
        public JsonResult Rule()
        {
            IActionInfoService actionInfo = new ActionInfoService();
            List<ActionInfo> ls = actionInfo.GetDatas().OrderBy("id asc").ToList();

            StateViewModel<List<ActionInfo>> state = new StateViewModel<List<ActionInfo>>
            {
                Code = 200,
                Data = ls
            };

            return new JsonResult(state);
        }
    }
}
