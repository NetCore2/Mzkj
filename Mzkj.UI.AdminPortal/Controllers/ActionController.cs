﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Mzkj.BLL;
using Mzkj.Common;
using Mzkj.Common.Model.Tree;
using Mzkj.Common.Tools;
using Mzkj.IBLL;
using Mzkj.Models;
using Mzkj.UI.AdminPortal.Models;
using SqlSugar;

namespace Mzkj.UI.AdminPortal.Controllers
{
    public class ActionController : BaseController
    {
        IActionInfoService actionInfo = new ActionInfoService();
        public IActionResult Index()
        {
            ViewData["limit"] = Config.pageCommon.PageSize;
            return View();
        }

        #region 获取权限列表Json/带查询数据
        [HttpGet]
        public IActionResult GetList()
        {
            
            string order = Request.Query["field"] + " " + Request.Query["order"];
            order = order.Trim() == "" ? "id asc" : order.Trim();

            ISugarQueryable<ActionInfo> u = actionInfo.GetDatas();
            Dictionary<string, string> dic = StringHelper.WhereDic(Request.QueryString.ToString());
            u = actionInfo.Search(u, dic);

            List<ActionInfo> ls = actionInfo.GetDatas().OrderBy(order).ToList();
            
            StateViewModel<List<ActionInfo>> state = new StateViewModel<List<ActionInfo>>
            {
                Code = 0,
                Data = ls
            };

            return new JsonResult(state);
        }
        
        #endregion

        #region 添加权限
        [HttpGet]
        public IActionResult Add()
        {
            ViewBag.act = actionInfo.GetList();
            return View();
        }

        [HttpPost]
        public IActionResult Add(ActionInfo model)
        {
            StateViewModel<String> state = new StateViewModel<String>() { Code = 5, Msg = "网络错误" };
            if (ModelState.IsValid)
            {
                IFormCollection form = Request.Form;
                model.CreateTime = SystemHelper.DateTimeToUnixTimestamp(DateTime.Now);
                model.UpdateTime = SystemHelper.DateTimeToUnixTimestamp(DateTime.Now);
                bool b = actionInfo.Add(model);
                if (b)
                {
                    state.Code = 1;
                    state.Msg = "添加成功";
                }
                else {
                    state.Code = 2;
                    state.Msg = "添加失败";
                }
            }
            else
            {
                state.Msg = "";
                foreach (var s in ModelState.Values)
                {
                    foreach (var p in s.Errors)
                    {
                        state.Msg += p.ErrorMessage + "<br>";
                    }
                }
            }
            return new JsonResult(state);
        }
        #endregion

        #region 编辑数据
        [HttpGet]
        public IActionResult Edit()
        {
            int id = int.Parse(Request.Query["id"]);
            ActionInfo u = actionInfo.Find(id);
            ViewBag.act = actionInfo.GetList();

            //tree
            List<ActionInfo> ls = actionInfo.GetDatas().ToList();
            List<CateTree> ls2 = new List<CateTree>();
            foreach (var d in ls)
            {
                ls2.Add(new CateTree()
                {
                    Id = d.Id,
                    Name = d.ActionName,
                    Url = d.Url,
                    Status = d.Status,
                    Delflag = d.Delflag,
                    Ismenu = d.Ismenu,
                    Pid = d.Pid,
                    CreateTime = d.CreateTime
                });
            }
            List<int> aid=ModelHelper.ChildIds(ls2, id);
            aid.Add(id);
            ViewBag.childIds = aid;
            return View(u);
        }


        public IActionResult Edit(ActionInfo model)
        {
            StateViewModel<String> state = new StateViewModel<String>() { Code = 5, Msg = "网络错误" };

            if (ModelState.IsValid)
            {
                model.UpdateTime = SystemHelper.DateTimeToUnixTimestamp(DateTime.Now);
                bool b = actionInfo.Save(model);
                if (b)
                {
                    state.Code = 1;
                    state.Msg = "编辑成功";
                }
                else
                {
                    state.Code = 2;
                    state.Msg = "编辑失败";
                }
            }
            else
            {
                //(ModelState.Values=foreach=>Errors)
                //var a = ModelState.Values;
                state.Msg = "";
                foreach (var s in ModelState.Values)
                {
                    foreach (var p in s.Errors)
                    {
                        state.Msg += p.ErrorMessage + "<br>";
                    }
                }
            }
            return new JsonResult(state);
        }
        #endregion

        #region 删除数据
        [HttpPost]
        public IActionResult Del(List<int> ids)
        {
            StateViewModel<String> state = new StateViewModel<String>() { Code = 5, Msg = "网络错误" };

            if (ids.Count > 0)
            {
                bool bo = actionInfo.Del(ids);
                if (bo)
                {
                    state.Code = 1;
                    state.Msg = "删除成功";
                }
                else
                {
                    state.Code = 2;
                    state.Msg = "删除失败";
                }
            }
            return new JsonResult(state);
        }
        #endregion
    }
}