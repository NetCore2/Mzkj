﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Mzkj.BLL;
using Mzkj.Common;
using Mzkj.Common.Tools;
using Mzkj.IBLL;
using Mzkj.Models;
using Mzkj.UI.AdminPortal.Models;
using SqlSugar;

namespace Mzkj.UI.AdminPortal.Controllers
{
    public class RoleController : Controller
    {
        IRoleService role = new RoleService();
        public IActionResult Index()
        {
            ViewData["limit"] = Config.pageCommon.PageSize;
            return View();
        }

        #region 获取权限列表Json/带查询数据
        [HttpGet]
        public IActionResult GetList()
        {

            int page = Convert.ToInt32(Request.Query["page"]);
            PageModel pageModel = new PageModel()
            {
                PageIndex = page,
                PageSize = Convert.ToInt32(Request.Query["limit"])
            };

            string order = Request.Query["field"] + " " + Request.Query["order"];
            order = order.Trim() == "" ? "id asc" : order.Trim();

            StateViewModel<List<Role>> state = new StateViewModel<List<Role>>();

            ISugarQueryable<Role> u = role.GetDatas();
            Dictionary<string, string> dic = StringHelper.WhereDic(Request.QueryString.ToString());
            u = role.Search(u, dic);

            List<Role> list = role.Search(u, page, order, pageModel);
            state.Code = 0;
            state.Count = u.Count();
            state.Data = list;

            return new JsonResult(state);
        }
        #endregion

        #region 添加权限
        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Add(Role model)
        {
            StateViewModel<String> state = new StateViewModel<String>() { Code = 5, Msg = "网络错误" };
            if (ModelState.IsValid)
            {
                IFormCollection form = Request.Form;
                model.CreateTime = SystemHelper.DateTimeToUnixTimestamp(DateTime.Now);
                model.UpdateTime = SystemHelper.DateTimeToUnixTimestamp(DateTime.Now);
                bool b = role.Add(model);
                if (b)
                {
                    state.Code = 1;
                    state.Msg = "添加成功";
                }
                else {
                    state.Code = 2;
                    state.Msg = "添加失败";
                }
            }
            else
            {
                state.Msg = "";
                foreach (var s in ModelState.Values)
                {
                    foreach (var p in s.Errors)
                    {
                        state.Msg += p.ErrorMessage + "<br>";
                    }
                }
            }
            return new JsonResult(state);
        }
        #endregion

        #region 编辑数据
        [HttpGet]
        public IActionResult Edit()
        {
            int id = int.Parse(Request.Query["id"]);
            Role u = role.Find(id);

            return View(u);
        }


        public IActionResult Edit(Role model)
        {
            StateViewModel<String> state = new StateViewModel<String>() { Code = 5, Msg = "网络错误" };

            if (ModelState.IsValid)
            {
                model.UpdateTime = SystemHelper.DateTimeToUnixTimestamp(DateTime.Now);
                bool b = role.Save(model);
                if (b)
                {
                    state.Code = 1;
                    state.Msg = "编辑成功";
                }
                else
                {
                    state.Code = 2;
                    state.Msg = "编辑失败";
                }
            }
            else
            {
                //(ModelState.Values=foreach=>Errors)
                //var a = ModelState.Values;
                state.Msg = "";
                foreach (var s in ModelState.Values)
                {
                    foreach (var p in s.Errors)
                    {
                        state.Msg += p.ErrorMessage + "<br>";
                    }
                }
            }
            return new JsonResult(state);
        }
        #endregion

        #region 删除数据
        [HttpPost]
        public IActionResult Del(List<int> ids)
        {
            StateViewModel<String> state = new StateViewModel<String>() { Code = 5, Msg = "网络错误" };

            if (ids.Count > 0)
            {
                bool bo = role.Del(ids);
                if (bo)
                {
                    state.Code = 1;
                    state.Msg = "删除成功";
                }
                else
                {
                    state.Code = 2;
                    state.Msg = "删除失败";
                }
            }
            return new JsonResult(state);
        }
        #endregion

        #region 设置用户
        [HttpGet]
        public IActionResult Set()
        {
            int id = int.Parse(Request.Query["id"]);
            Role u = role.Find(id);
            ViewBag.role = u;
            ActionInfoService action = new ActionInfoService();
            List<ActionInfo> r = action.GetList();
            ViewBag.action = r;

            RoleActionService roleAction = new RoleActionService();
            var ur = roleAction.GetList(s => s.Rid == id);
            
            List<int> raction = new List<int>();
            foreach (var rs in ur)
            {
                raction.Add(rs.Aid);
            }
            ViewBag.aids = raction;
            return View();
        }

        public IActionResult Set(int rid)
        {
            StateViewModel<String> state = new StateViewModel<String>() { Code = 5, Msg = "网络错误" };
            List<RoleAction> setAction = new List<RoleAction>();
            foreach (var k in Request.Form.Keys)
            {
                if (k.StartsWith("aid"))
                {
                    int aid = Convert.ToInt32((k.Replace("aid[", "")).Replace("]", ""));
                    setAction.Add(new RoleAction { Rid=rid,Aid=aid});
                }
            }
            bool b = role.SetAction(rid, setAction);
            if (b)
            {
                state.Code = 1;
                state.Msg = "设置角色权限成功!";
            }
            else
            {
                state.Code = 2;
                state.Msg = "设置角色权限失败!";
            }
            return new JsonResult(state);
        }
        #endregion
    }
}