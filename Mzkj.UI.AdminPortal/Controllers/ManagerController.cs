﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Mzkj.BLL;
using Mzkj.Common;
using Mzkj.Common.Tools;
using Mzkj.IBLL;
using Mzkj.Models;
using Mzkj.UI.AdminPortal.Models;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Mzkj.UI.AdminPortal.Controllers
{
    /*管理员*/
    public class ManagerController : BaseController
    {
        IUserService user = new UserService();
        UserInfoService userInfo = new UserInfoService();
        public IActionResult Index()
        {
            ViewData["limit"] = Config.pageCommon.PageSize;
            return View();
        }

        #region 获取用户列表Json/带查询数据
        [HttpGet]
        public IActionResult GetList()
        {
            
            int page = Convert.ToInt32(Request.Query["page"]);
            PageModel pageModel = new PageModel() {
                PageIndex = page,
                PageSize = Convert.ToInt32(Request.Query["limit"])
            };

            string order = Request.Query["field"] + " " + Request.Query["order"];
            order = order.Trim()=="" ? "id asc": order.Trim();

            StateViewModel<List<User>> state = new StateViewModel<List<User>>();

            ISugarQueryable<User> u= user.GetDatas(it => it.ManagerNum == 1);
            Dictionary<string, string> dic = StringHelper.WhereDic(Request.QueryString.ToString());
            u=user.Search(u, dic);

            List<User> list = user.Search(u,page,order,pageModel);
            state.Code = 0;
            state.Count = u.Count();// user.Count(it => it.ManagerNum == 1);
            state.Data = list;

            return new JsonResult(state);
        }
        #endregion

        #region 添加用户
        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Add(User model)
        {
            StateViewModel<String> state = new StateViewModel<String>() { Code = 5, Msg = "网络错误" };

            if (string.IsNullOrEmpty(model.Password))
            {
                state.Msg = "密码不能为空!";
                return new JsonResult(state);
            }
            if (ModelState.IsValid)
            {
                IFormCollection form = Request.Form;
                if (!string.IsNullOrWhiteSpace(form["email"])) {
                    if (!ValidateRegex.Email(form["email"])) {
                        state.Msg = "邮箱格式不正确!";
                        return new JsonResult(state);
                    }
                }
                if (!string.IsNullOrWhiteSpace(form["phone"]))
                {
                    if (!ValidateRegex.Phone(form["phone"]))
                    {
                        state.Msg = "电话号码不正确!";
                        return new JsonResult(state);
                    }
                }
                UserInfo userInfo = new UserInfo
                {
                    Email = form["email"],
                    Phone = form["phone"],
                    Sex = Convert.ToByte(form["sex"]),
                    CreateTime = SystemHelper.DateTimeToUnixTimestamp(DateTime.Now),
                    UpdateTime = SystemHelper.DateTimeToUnixTimestamp(DateTime.Now)
                };
                model.ManagerNum = 1;
                model.Delflag = 0;
                model.CreateTime = SystemHelper.DateTimeToUnixTimestamp(DateTime.Now);
                model.UpdateTime = SystemHelper.DateTimeToUnixTimestamp(DateTime.Now);
                int u = user.Add(model, userInfo, out string msg);
                state.Code = u;
                state.Msg = msg;
            }
            else {
                //(ModelState.Values=foreach=>Errors)
                //var a = ModelState.Values;
                state.Msg = "";
                foreach (var s in ModelState.Values)
                {
                    foreach (var p in s.Errors)
                    {
                        state.Msg += p.ErrorMessage + "<br>";
                    }
                }
            }
            return new JsonResult(state);
        }
        #endregion

        #region 编辑用户
        [HttpGet]
        public IActionResult Edit()
        {
            int id = int.Parse(Request.Query["id"]);
            User u = user.Find(id);
            UserInfo uf = userInfo.First(it => it.Uid == id);

            return View(Tuple.Create(u, uf));
        }

        
        public IActionResult Edit(User model)
        {
            StateViewModel<String> state = new StateViewModel<String>() { Code = 5, Msg = "网络错误" };

            if (ModelState.IsValid)
            {
                IFormCollection form = Request.Form;
                if (!string.IsNullOrWhiteSpace(form["email"]))
                {
                    if (!ValidateRegex.Email(form["email"]))
                    {
                        state.Msg = "邮箱格式不正确!";
                        return new JsonResult(state);
                    }
                }
                if (!string.IsNullOrWhiteSpace(form["phone"]))
                {
                    if (!ValidateRegex.Phone(form["phone"]))
                    {
                        state.Msg = "电话号码不正确!";
                        return new JsonResult(state);
                    }
                }
                UserInfo userInfo = new UserInfo
                {
                    Id = int.Parse(form["id"]),
                    Email = form["email"],
                    Phone = form["phone"],
                    Sex = Convert.ToByte(form["sex"]),
                    UpdateTime = SystemHelper.DateTimeToUnixTimestamp(DateTime.Now)
                };
                model.UpdateTime = SystemHelper.DateTimeToUnixTimestamp(DateTime.Now);
                int u = user.Edit(model, userInfo, out string msg);
                state.Code = u;
                state.Msg = msg;
            }
            else
            {
                //(ModelState.Values=foreach=>Errors)
                //var a = ModelState.Values;
                state.Msg = "";
                foreach (var s in ModelState.Values)
                {
                    foreach (var p in s.Errors)
                    {
                        state.Msg += p.ErrorMessage + "<br>";
                    }
                }
            }
            return new JsonResult(state);
        }
        #endregion

        #region 删除用户
        [HttpPost]
        public IActionResult Del(List<int> ids){
            StateViewModel<String> state = new StateViewModel<String>() { Code = 5, Msg = "网络错误" };

            if (ids.Count > 0)
            {
                bool b = ids.Remove(1);
                if (b && ids.Count <= 0)
                {
                    state.Code = 2;
                    state.Msg = "禁止删除该用户!";
                }
                else {
                    int u = user.Del(ids, out string msg);
                    state.Code = u;
                    state.Msg = msg;
                }
            }
            return new JsonResult(state);
        }
        #endregion

        #region 设置用户
        [HttpGet]
        public IActionResult Set()
        {
            int id = int.Parse(Request.Query["id"]);
            User u = user.Find(id);
            ViewBag.user = u;
            RoleService role = new RoleService();
            List<Role> r=role.GetList();
            ViewBag.role = r;

            UserRoleService userRole = new UserRoleService();
            var ur=userRole.GetList(it => it.Uid == id);
            List<int> urole = new List<int>();
            foreach (var rs in ur)
            {
                urole.Add(rs.Rid);
            }
            ViewBag.rids = urole;
            return View();
        }
        
        public IActionResult Set(int uid)
        {
            StateViewModel<String> state = new StateViewModel<String>() { Code = 5, Msg = "网络错误" };
            List<UserRole> setRole = new List<UserRole>();
            foreach (var k in Request.Form.Keys)
            {
                if (k.StartsWith("rid"))
                {
                    int rid = Convert.ToInt32((k.Replace("rid[", "")).Replace("]",""));
                    setRole.Add(new UserRole {Uid= uid, Rid=rid});
                }
            }
            bool b=user.SetRole(uid, setRole);
            if (b)
            {
                state.Code = 1;
                state.Msg = "设置用户角色成功!";
            }
            else
            {
                state.Code = 2;
                state.Msg = "设置用户角色失败!";
            }
            return new JsonResult(state);
        }
        #endregion

        #region 设置用户特殊权限
        public IActionResult SetAction()
        {
            int id = int.Parse(Request.Query["id"]);
            User u = user.Find(id);
            ViewBag.user = u;
            //
            IActionInfoService action = new ActionInfoService();
            List<ActionInfo> ac=action.GetList();


            return View(ac);
        }
        #endregion
    }
}
