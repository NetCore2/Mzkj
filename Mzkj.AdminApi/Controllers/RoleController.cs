﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Mzkj.AdminApi.ViewModels;
using Mzkj.BLL;
using Mzkj.Common.Tools;
using Mzkj.IBLL;
using Mzkj.Models;
using SqlSugar;

namespace Mzkj.AdminApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Role")]
    public class RoleController : BaseController
    {
        IRoleService role = new RoleService();

        // GET: api/Role
        [HttpGet]
        public JsonResult Get()
        {
            int page = Convert.ToInt32(Request.Query["page"]);
            PageModel pageModel = new PageModel()
            {
                PageIndex = page,
                PageSize = Convert.ToInt32(Request.Query["limit"])
            };

            string order = Request.Query["field"] + " " + Request.Query["order"];
            order = order.Trim() == "" ? "id asc" : order.Trim();

            StateViewModel<List<Role>> state = new StateViewModel<List<Role>>();

            ISugarQueryable<Role> u = role.GetDatas();
            Dictionary<string, string> dic = StringHelper.WhereDic(Request.QueryString.ToString());
            u = role.Search(u, dic);

            List<Role> list = role.Search(u, page, order, pageModel);
            state.Code = 200;
            state.Total = u.Count();
            state.Data = list;

            return new JsonResult(state);
        }
                
        // POST: api/Role
        [HttpPost]
        public JsonResult Post(Role model)
        {
            StateViewModel<String> state = new StateViewModel<String>() { Code = 5, Msg = "网络错误" };
            if (ModelState.IsValid)
            {
                IFormCollection form = Request.Form;
                model.CreateTime = SystemHelper.DateTimeToUnixTimestamp(DateTime.Now);
                model.UpdateTime = SystemHelper.DateTimeToUnixTimestamp(DateTime.Now);
                bool b = role.Add(model);
                if (b)
                {
                    state.Code = 200;
                    state.Msg = "添加成功";
                }
                else
                {
                    state.Code = 300;
                    state.Msg = "添加失败";
                }
            }
            else
            {
                state.Msg = "";
                foreach (var s in ModelState.Values)
                {
                    foreach (var p in s.Errors)
                    {
                        state.Msg += p.ErrorMessage + ";";
                    }
                }
            }
            return new JsonResult(state);
        }
        
        // PUT: api/Role
        [HttpPut]
        public JsonResult Put(Role model)
        {
            StateViewModel<String> state = new StateViewModel<String>() { Code = 400, Msg = "网络错误" };
            //Redirect(Url.Action("ECode", "Errors", new { Msg = "没有操作权限" }));
            if (ModelState.IsValid)
            {
                model.UpdateTime = SystemHelper.DateTimeToUnixTimestamp(DateTime.Now);
                bool b = role.Save(model);
                if (b)
                {
                    state.Code = 200;
                    state.Msg = "编辑成功";
                }
                else
                {
                    state.Code = 300;
                    state.Msg = "编辑失败";
                }
            }
            else
            {
                //(ModelState.Values=foreach=>Errors)
                //var a = ModelState.Values;
                state.Msg = "";
                foreach (var s in ModelState.Values)
                {
                    foreach (var p in s.Errors)
                    {
                        state.Msg += p.ErrorMessage + ";";
                    }
                }
            }
            return new JsonResult(state);
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        public JsonResult Delete(List<int> ids)
        {
            StateViewModel<String> state = new StateViewModel<String>() { Code = 400, Msg = "网络错误" };

            if (ids.Count > 0)
            {
                bool bo = role.Del(ids);
                if (bo)
                {
                    state.Code = 200;
                    state.Msg = "删除成功";
                }
                else
                {
                    state.Code = 300;
                    state.Msg = "删除失败";
                }
            }
            return new JsonResult(state);
        }



        //设置
        [HttpPatch]
        public JsonResult Patch(int rid,int[] aid)
        {
            StateViewModel<String> state = new StateViewModel<String>() { Code = 400, Msg = "网络错误" };
            List<RoleAction> setAction = new List<RoleAction>();
            /*foreach (var k in Request.Form.Keys)
            {
                if (k.StartsWith("aid"))
                {
                    int aid = Convert.ToInt32((k.Replace("aid[", "")).Replace("]", ""));
                    setAction.Add(new RoleAction { Rid = rid, Aid = aid });
                }
            }*/
            for (int i = 0; i < aid.Length; i++)
            {
                setAction.Add(new RoleAction { Rid = rid, Aid = aid[i] });
            }
            bool b = role.SetAction(rid, setAction);
            if (b)
            {
                state.Code = 200;
                state.Msg = "设置角色权限成功!";
            }
            else
            {
                state.Code = 300;
                state.Msg = "设置角色权限失败!";
            }
            return new JsonResult(state);
        }

        [HttpPatch("{sid}")]
        public JsonResult Patch(string sid)
        {
            int id = Convert.ToInt32(sid);

            //角色对应的权限id
            RoleActionService roleAction = new RoleActionService();
            var ur = roleAction.GetList(s => s.Rid == id);
            List<int> raction = new List<int>();
            foreach (var rs in ur)
            {
                raction.Add(rs.Aid);
            }
            //所有权限列表
            ActionInfoService action = new ActionInfoService();
            List<ActionInfo> rlist = action.GetList();


            return new JsonResult(Tools.Xtree.Ctree(rlist, 0, raction));
        }
        /*[HttpPatch("{sid}")]
        public JsonResult Patch(int sid)
        {
            int id = Convert.ToInt32(sid);

            //角色对应的权限id
            RoleActionService roleAction = new RoleActionService();
            var ur = roleAction.GetList(s => s.Rid == id);
            List<int> raction = new List<int>();
            foreach (var rs in ur)
            {
                raction.Add(rs.Aid);
            }
            //所有权限列表
            ActionInfoService action = new ActionInfoService();
            List<ActionInfo> rlist = action.GetList();


            return new JsonResult(Tools.Xtree.Ctree(rlist, 0, raction));
        }*/
    }
}
