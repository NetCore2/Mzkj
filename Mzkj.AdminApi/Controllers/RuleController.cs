﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Mzkj.AdminApi.ViewModels;
using Mzkj.BLL;
using Mzkj.Common.Tools;
using Mzkj.IBLL;
using Mzkj.Models;
using SqlSugar;

namespace Mzkj.AdminApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Rule")]
    public class RuleController : BaseController
    {
        IActionInfoService actionInfo = new ActionInfoService();
        // GET: api/Rule
        [HttpGet]
        public JsonResult Get()
        {
            string order = Request.Query["field"] + " " + Request.Query["order"];
            order = order.Trim() == "" ? "id asc" : order.Trim();

            /*ISugarQueryable<ActionInfo> u = actionInfo.GetDatas();
            Dictionary<string, string> dic = StringHelper.WhereDic(Request.QueryString.ToString());
            u = actionInfo.Search(u, dic);*/

            List<ActionInfo> ls = actionInfo.GetDatas().OrderBy(order).ToList();
            
            List<Object> list = new List<Object>();

            ActionInfo oneInfo;
            foreach (var v in ls)
            {
                oneInfo = actionInfo.Find(v.Pid);
                string ParentName = "顶级规则";
                if (oneInfo != null) {
                    ParentName = oneInfo.ActionName;
                }
                list.Add(new {
                    v.ActionName,
                    v.Dirname,
                    v.CreateTime,
                    v.Delflag,
                    v.Httpmet,
                    v.Id,
                    v.Ismenu,
                    v.Pid,
                    v.Remark,
                    v.Sort,
                    v.Status,
                    v.Url,
                    ParentName
                });
            }
            StateViewModel<List<Object>> state = new StateViewModel<List<Object>>
            {
                Code = 200,
                Data = list
            };

            return new JsonResult(state);
        }
        
        // POST: api/Rule
        [HttpPost]
        public JsonResult Post(ActionInfo model)
        {
            StateViewModel<String> state = new StateViewModel<String>() { Code = 400, Msg = "网络错误" };
            if (ModelState.IsValid)
            {
                bool b = actionInfo.Add(model);
                if (b)
                {
                    state.Code = 200;
                    state.Msg = "添加成功";
                }
                else
                {
                    state.Code = 300;
                    state.Msg = "添加失败";
                }
            }
            else
            {
                state.Msg = "";
                foreach (var s in ModelState.Values)
                {
                    foreach (var p in s.Errors)
                    {
                        state.Msg += p.ErrorMessage + ";";
                    }
                }
            }
            return new JsonResult(state);
        }
        
        // PUT: api/Rule/5
        [HttpPut]
        public JsonResult Put(ActionInfo model)
        {
            StateViewModel<String> state = new StateViewModel<String>() { Code = 400, Msg = "网络错误" };

            if (ModelState.IsValid)
            {
                if (model.Ismenu.ToString() == "" && model.Ismenu.ToString() == null) {
                    model.Ismenu = 0;
                }

                bool b = actionInfo.Save(model);
                if (b)
                {
                    state.Code = 200;
                    state.Msg = "编辑成功";
                }
                else
                {
                    state.Code = 300;
                    state.Msg = "编辑失败";
                }
            }
            else
            {
                state.Msg = "";
                foreach (var s in ModelState.Values)
                {
                    foreach (var p in s.Errors)
                    {
                        state.Msg += p.ErrorMessage + ";";
                    }
                }
            }
            return new JsonResult(state);
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        public JsonResult Delete(List<int> ids)
        {
            StateViewModel<String> state = new StateViewModel<String>() { Code = 400, Msg = "网络错误" };

            if (ids.Count > 0)
            {
                bool bo = actionInfo.Del(ids);
                if (bo)
                {
                    state.Code = 200;
                    state.Msg = "删除成功";
                }
                else
                {
                    state.Code = 300;
                    state.Msg = "删除失败";
                }
            }
            return new JsonResult(state);
        }
    }
}
