﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Mzkj.BLL;
using Mzkj.Common.Model.Tree;
using Mzkj.Common.Tools;
using Mzkj.IBLL;
using Mzkj.Models;
using System;
using System.Collections.Generic;

namespace Mzkj.UI.AdminApi.Controllers
{
    [Authorize]
    [Route("[controller]/[Action]")]
    public class TreeController:Controller
    {
        [HttpGet]
        public JsonResult Rule(int id)
        {
            IActionInfoService actionInfo = new ActionInfoService();

            int cid;
            if (id.ToString()!="" && id.ToString()!=null) {
                cid = id;
            }
            else
            {
                cid = 0;
            }

            //tree
            List<ActionInfo> ls = actionInfo.GetDatas().ToList();
            List<CateTree> ls2 = new List<CateTree>();
            foreach (var d in ls)
            {
                ls2.Add(new CateTree()
                {
                    Id = d.Id,
                    Name = d.ActionName,
                    Url = d.Url,
                    Status = d.Status,
                    Delflag = d.Delflag,
                    Ismenu = d.Ismenu,
                    Pid = d.Pid,
                    CreateTime = d.CreateTime
                });
            }
            List<int> aid = new List<int>{ };
            if (cid != 0)
            {
                aid = ModelHelper.ChildIds(ls2, cid);
                aid.Add(cid);
            }

            List<ActionInfo> tree = actionInfo.GetDatas().Where(it =>it.Delflag != 1 && !aid.Contains(it.Id)).OrderBy("id asc").ToList();

            List<Object> tree2 = new List<object>
            {
                new { Id = 0, Name = "顶级规则" }
            };
            foreach (var item in tree)
            {
                tree2.Add(new { item.Id, Name = item.ActionName,item.ActionName,item.CreateTime, ParentId=item.Pid,item.Httpmet,item.UpdateTime });
            }
            return new JsonResult(tree2);
        }
        
        [HttpGet]
        public JsonResult GetMenu()
        {
            string uid = Common.Cache.CookiesHelper.Get("userLoginId", HttpContext);
            if (uid == null)
            {
                return new JsonResult(new { Code = 300, Msg = "请登陆后操作!" });
            }
            string userStr = Common.Cache.CacheHelper.GetCache(uid);
            if (userStr == null)
            {
                return new JsonResult(new { Code=300,Msg="非法用户!" });
            }
            User userInfo = StringHelper.ParseFormJson<User>(userStr);
            List<ActionInfo> menu=Mzkj.AdminApi.Tools.Menu.GetMenuOne(userInfo.Id);
            return new JsonResult(new { Code=200,Msg="获取当前用户菜单" ,Menu= menu });
        }

        [HttpGet]
        public JsonResult GetMenuSe() {
            string uid = Common.Cache.CookiesHelper.Get("userLoginId", HttpContext);
            if (uid == null)
            {
                return new JsonResult(new { Code = 300, Msg = "请登陆后操作!" });
            }
            string userStr = Common.Cache.CacheHelper.GetCache(uid);
            if (userStr == null)
            {
                return new JsonResult(new { Code = 300, Msg = "非法用户!" });
            }
            User userInfo = StringHelper.ParseFormJson<User>(userStr);
            Dictionary<string,object> menu = Mzkj.AdminApi.Tools.Menu.GetMenuSe(userInfo.Id);
            return new JsonResult(new { Code = 200, Msg = "获取当前用户菜单", Menu = menu });
        }
    }
}
