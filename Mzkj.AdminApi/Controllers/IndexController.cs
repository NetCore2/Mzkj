﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Mzkj.AdminApi.ViewModels;
using Mzkj.BLL;
using Mzkj.IBLL;

namespace Mzkj.AdminApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Index")]
    public class IndexController : BaseController
    {
        [HttpGet]
        public JsonResult Get()
        {
            StateViewModel<StateIndex> state = new StateViewModel<StateIndex>() { Code = 200, Msg = "" };
            IUserService userService = new UserService();
            int userCount = userService.Count(
                it => it.Delflag == 0 && it.ManagerNum == 0
                );
            state.Data = new StateIndex{UCount=userCount};
            return new JsonResult(state);
        }
    }
}