﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Mzkj.BLL;
using Mzkj.IBLL;
using Mzkj.Models;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Mzkj.AdminApi.Controllers
{
    [Authorize]
    public class BaseController:Controller
    {
        public bool IsCheckLogin = true;

        public User LoginUser { get; set; }

        public override void OnActionExecuting(ActionExecutingContext context)
        {

            if (IsCheckLogin)
            {
                //使用Cookies+redis
                Request.Cookies.TryGetValue("userLoginId", out string userGuid);
                if (userGuid == null)
                {
                    //返回登录
                    context.Result = new JsonResult(new { Msg = "非法登录"});
                    return;
                    /*context.Result = new RedirectResult(Url.Action("ECode", "Errors", new { Msg = "非法登录"}));
                    return;*/
                }
                string token = Common.Cache.CacheHelper.GetCache("token");
                if (token == null)
                {
                    //返回登录
                    context.Result = new JsonResult(new { Msg = "没有授权" });
                    return;
                    /*context.Result = new RedirectResult(Url.Action("ECode", "Errors", new { Msg = "没有授权" }));
                    return;*/
                }

                string user = Common.Cache.CacheHelper.GetCache(userGuid);
                if (user == null)
                {
                    //用户长时间不操作超时, 返回登录
                    context.Result = new JsonResult(new { Msg = "用户登录超时" });
                    return;
                    /*context.Result = new RedirectResult(Url.Action("ECode", "Errors", new { Msg = "用户登录超时" }));
                    return;*/
                }
                User userInfo = Common.Tools.StringHelper.ParseFormJson<User>(user);
                LoginUser = userInfo;
                //滑动窗口机制
                Common.Cache.CacheHelper.SetCache(userGuid, user, Common.Config.minutes);
                Common.Cache.CacheHelper.AddCache("token", token, Common.Config.minutes);


                //校验权限
                if (userInfo.Username == "admin")
                {
                    return;
                }
                //权限的连接
                string url = Request.Path;
                string s = Regex.Replace(url, @"^/\w+/\w+/", "");

                //Rsetful Api正则替换.如api/role/1去掉1

                //Response.Redirect("==+" + url);
                //string url = context.HttpContext.Request.Path;
                url = url.Replace("/"+s, "");
                url = url.Substring(1);
                string httpMethod = context.HttpContext.Request.Method.ToLower();

                IActionInfoService actionInfo = new ActionInfoService();
                var ac = actionInfo.GetDatas().Where(it => it.Url.ToLower() == url.ToLower() && it.Httpmet.ToLower() == httpMethod).First();

                if (ac == null)
                {
                    context.Result = new JsonResult(new { Msg = "不存在该操作权限" });
                    return;
                    /*context.Result = new RedirectResult(Url.Action("ECode", "Errors", new { Msg = "不存在该操作权限" }));
                    return;*/
                }

                IUserRoleService userRole = new UserRoleService();
                IRoleActionService roleAction = new RoleActionService();
                //查询登录用户的角色
                List<UserRole> luserRole = userRole.GetDatas().Where(it => it.Uid == userInfo.Id).ToList();
                List<int> rids = new List<int>();
                for (int i = 0; i < luserRole.Count; i++)
                {
                    rids.Add(luserRole[i].Rid);
                }

                //根据角色查询权限Id
                List<RoleAction> lroleAction = roleAction.GetDatas().Where(it => rids.Contains(it.Rid)).ToList();
                List<int> aids = new List<int>();
                for (int i = 0; i < lroleAction.Count; i++)
                {
                    aids.Add(lroleAction[i].Aid);
                }
                //根据权限ID查询权限
                List<ActionInfo> a = actionInfo.GetDatas().Where(it => aids.Contains(it.Id) && it.Url.ToLower() == url.ToLower() && it.Httpmet.ToLower() == httpMethod).ToList();
                if (a.Count <= 0)
                {
                    context.Result = new JsonResult(new { Msg = "没有操作权限" });
                    return;
                    /*context.Result = new RedirectResult(Url.Action("ECode", "Errors", new { Msg = "没有操作权限" }));
                    return;*/
                }
            }

            base.OnActionExecuting(context);
        }
    }
}
