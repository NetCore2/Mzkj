﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Mzkj.AdminApi.ViewModels;
using Mzkj.BLL;
using Mzkj.Common.Tools;
using Mzkj.IBLL;
using Mzkj.Models;
using SqlSugar;

namespace Mzkj.AdminApi.Controllers
{
    /*管理员*/
    [Produces("application/json")]
    [Route("api/Manager")]
    public class ManagerController : BaseController
    {
        IUserService user = new UserService();
        IUserInfoService userInfo = new UserInfoService();

        // GET: api/Manager
        [HttpGet]
        public JsonResult Get()
        {
            int page = Convert.ToInt32(Request.Query["page"]);
            PageModel pageModel = new PageModel()
            {
                PageIndex = page,
                PageSize = Convert.ToInt32(Request.Query["limit"])
            };

            string order = Request.Query["field"] + " " + Request.Query["order"];
            order = order.Trim() == "" ? "id asc" : order.Trim();

            StateViewModel<List<User>> state = new StateViewModel<List<User>>();

            ISugarQueryable<User> u = user.GetDatas(it => it.ManagerNum == 1);
            Dictionary<string, string> dic = StringHelper.WhereDic(Request.QueryString.ToString());
            u = user.Search(u, dic);

            List<User> list = user.Search(u, page, order, pageModel);
            state.Code = 200;
            state.Total = u.Count();// user.Count(it => it.ManagerNum == 1);
            state.Data = list;

            return new JsonResult(state);
        }
        
        // GET api/values/5
        [HttpGet("{id}")]
        public JsonResult Get(int id)
        {
            User u = user.Find(id);
            UserInfo uf = userInfo.First(it => it.Uid == id);
            Object users = new {
                User=u,
                UserInfo=uf
            };
            //Tuple.Create(u, uf)
            return new JsonResult(users);
        }

        // POST: api/Manager
        [HttpPost]
        public JsonResult Post(User model)
        {
            StateViewModel<String> state = new StateViewModel<String>() { Code = 400, Msg = "网络错误" };

            if (string.IsNullOrEmpty(model.Password))
            {
                state.Msg = "密码不能为空!";
                return new JsonResult(state);
            }
            if (ModelState.IsValid)
            {
                IFormCollection form = Request.Form;
                if (!string.IsNullOrWhiteSpace(form["email"]))
                {
                    if (!ValidateRegex.Email(form["email"]))
                    {
                        state.Msg = "邮箱格式不正确!";
                        return new JsonResult(state);
                    }
                }
                if (!string.IsNullOrWhiteSpace(form["phone"]))
                {
                    if (!ValidateRegex.Phone(form["phone"]))
                    {
                        state.Msg = "电话号码不正确!";
                        return new JsonResult(state);
                    }
                }
                UserInfo userInfo = new UserInfo
                {
                    Email = form["email"],
                    Phone = form["phone"],
                    Sex = Convert.ToByte(form["sex"])
                };
                model.ManagerNum = 1;
                int u = user.Add(model, userInfo, out string msg);
                state.Code = u;
                state.Msg = msg;
            }
            else
            {
                //(ModelState.Values=foreach=>Errors)
                //var a = ModelState.Values;
                state.Msg = "";
                foreach (var s in ModelState.Values)
                {
                    foreach (var p in s.Errors)
                    {
                        state.Msg += p.ErrorMessage + ";";
                    }
                }
            }
            return new JsonResult(state);
        }
        
        // PUT: api/Manager/5
        [HttpPut]
        public JsonResult Put(User model)
        {
            StateViewModel<String> state = new StateViewModel<String>() { Code = 400, Msg = "网络错误" };

            if (ModelState.IsValid)
            {
                IFormCollection form = Request.Form;
                if (!string.IsNullOrWhiteSpace(form["email"]))
                {
                    if (!ValidateRegex.Email(form["email"]))
                    {
                        state.Msg = "邮箱格式不正确!";
                        return new JsonResult(state);
                    }
                }
                if (!string.IsNullOrWhiteSpace(form["phone"]))
                {
                    if (!ValidateRegex.Phone(form["phone"]))
                    {
                        state.Msg = "电话号码不正确!";
                        return new JsonResult(state);
                    }
                }
                UserInfo userInfo = new UserInfo
                {
                    Id = int.Parse(form["id"]),
                    Email = form["email"],
                    Phone = form["phone"],
                    Sex = Convert.ToByte(form["sex"]),
                    CreateTime = model.CreateTime
                };
                model.ManagerNum = 1;
                int u = user.Edit(model, userInfo, out string msg);
                state.Code = u;
                state.Msg = msg;
            }
            else
            {
                state.Msg = "";
                foreach (var s in ModelState.Values)
                {
                    foreach (var p in s.Errors)
                    {
                        state.Msg += p.ErrorMessage + ";";
                    }
                }
            }
            return new JsonResult(state);
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        public JsonResult Delete(List<int> ids)
        {
            StateViewModel<String> state = new StateViewModel<String>() { Code = 400, Msg = "网络错误" };

            if (ids.Count > 0)
            {
                bool b = ids.Remove(1);
                if (b && ids.Count <= 0)
                {
                    state.Code = 300;
                    state.Msg = "禁止删除该用户!";
                }
                else
                {
                    int u = user.Del(ids, out string msg);
                    state.Code = u;
                    state.Msg = msg;
                }
            }
            return new JsonResult(state);
        }

        [HttpPatch]
        public JsonResult Patch(int uid) {
            StateViewModel<String> state = new StateViewModel<String>() { Code = 400, Msg = "网络错误" };
            List<UserRole> setRole = new List<UserRole>();
            foreach (var k in Request.Form.Keys)
            {
                if (k.StartsWith("rid"))
                {
                    int rid = Convert.ToInt32((k.Replace("rid[", "")).Replace("]", ""));
                    setRole.Add(new UserRole { Uid = uid, Rid = rid });
                }
            }
            bool b = user.SetRole(uid, setRole);
            if (b)
            {
                state.Code = 200;
                state.Msg = "设置用户角色成功!";
            }
            else
            {
                state.Code = 300;
                state.Msg = "设置用户角色失败!";
            }
            return new JsonResult(state);
        }

        [HttpPatch("{sid}")]
        public JsonResult Patch(string sid)
        {
            int id = Convert.ToInt32(sid);
            //User u = user.Find(id);
            RoleService role = new RoleService();
            List<Role> r = role.GetList();

            UserRoleService userRole = new UserRoleService();
            var ur = userRole.GetList(it => it.Uid == id);
            List<int> urole = new List<int>();
            foreach (var rs in ur)
            {
                urole.Add(rs.Rid);
            }

            Object roles = new
            {
                //user = u,
                role = r,
                rids = urole
            };

            return new JsonResult(roles);
        }
    }
}
