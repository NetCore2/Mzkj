﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Mzkj.Common.Cache;
using Mzkj.Common.Tools;

namespace Mzkj.UI.AdminApi.Controllers
{
    [Route("[controller]/[Action]")]
    public class ToolController : Controller
    {
        /// <summary>
        /// 验证码
        /// </summary>
        /// <returns></returns>
        public IActionResult Vcode()
        {
            System.IO.MemoryStream ms = (new ValidateCodeTools()).Create(out string code);
            CacheHelper.AddCache("LoginCode", code,5);
            Response.Body.Dispose();
            return File(ms.ToArray(), @"image/png");
        }
        
    }
}