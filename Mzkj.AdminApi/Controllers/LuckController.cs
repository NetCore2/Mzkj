﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Mzkj.AdminApi.ViewModels;
using Mzkj.BLL;
using Mzkj.Common.Tools;
using Mzkj.IBLL;
using Mzkj.Models;

namespace Mzkj.AdminApi.Controllers
{
    [Route("[controller]/[Action]")]
    [Authorize]
    public class LuckController : Controller
    {
        [HttpGet]
        public JsonResult UserInfo()
        {
            StateViewModel<string> userState = Tools.Auth.GetUserString(HttpContext);
            if (userState.Code != 200)
            {
                return new JsonResult(new { userState.Code, userState.Msg });
            }
            User user = StringHelper.ParseFormJson<User>(userState.Data);

            IUserInfoService userInfoService= new UserInfoService();
            UserInfo userInfo = userInfoService.First(it=>it.Uid==user.Id);

            object userAll = new {
                //user.Id, 不保存id
                user.Nickname,
                user.Username,
                userInfo.FaceImg,
                userInfo.Sex,
                userInfo.Phone,
                userInfo.Email,
                user.CreateTime
            };

            return new JsonResult(userAll);
        }

        [HttpPost]
        public JsonResult UploadFace([FromServices]IHostingEnvironment environment)
        {

            StateViewModel<StateFace> state = new StateViewModel<StateFace>() { Code=400,Msg="网络错误!"};
            var files = Request.Form.Files;
            var form = Request.Form;
            string dirname = form.ContainsKey("dirname") ? form["dirname"].ToString() : "default";
            StateFace data=UploadHelper.UploadOne(files,10, form["dirname"]);
            if (data.Src != null) { 
                state.Code = 200;
                state.Msg = "上传成功";
                state.Data = data;
            }
            else{
                state.Code = 300;
                state.Msg = data.Title;
            }
            return new JsonResult(state);
        }

        #region 上传备用
        /*public async Task<JsonResult> UploadImg([FromServices]IHostingEnvironment environment)
        {
            //[FromServices]IHostingEnvironment environment
            string path = string.Empty;     //文件全路径
            string filePath = string.Empty; //文件夹路径
            string fileExt = string.Empty;   //不带点后缀名
            var files = Request.Form.Files;
            long size = files.Sum(f => f.Length);
            //string webRootPath = environment.WebRootPath;
            string webRootPath = environment.WebRootPath;
            Random rd = new Random();
            string curName = "user";    //当前文件夹

            foreach (var formFile in files)
            {
                if (formFile.Length > 0)
                {
                    fileExt = SystemHelper.GetFileExt(formFile.FileName);
                    filePath = Path.Combine(webRootPath, "Upload", curName, DateTime.Now.ToString("yyyyMMdd"));
                    path = Path.Combine(filePath, DateTime.Now.ToString("HHmmss") + rd.Next(1000, 9999).ToString() + "." + fileExt);

                    long fileSize = formFile.Length; //获得文件大小，以字节为单位
                    if (!Directory.Exists(filePath))
                    {
                        Directory.CreateDirectory(filePath);
                    }
                    using (var stream = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite))
                    {
                        await formFile.CopyToAsync(stream);
                    }
                }

            }

            return new JsonResult(new { Res = Request.Form.Files, filePath, size });
        }*/
        #endregion
    }
}