﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Mzkj.AdminApi.ViewModels;
using Mzkj.BLL;
using Mzkj.Common.Tools;
using Mzkj.IBLL;
using Mzkj.Models;

namespace Mzkj.AdminApi.Controllers
{
    public class OtherController : BaseController
    {
        #region 个人密码修改
        public JsonResult ChangePassword()
        {
            StateViewModel<string> state = new StateViewModel<string>() { Code = 300, Msg = "网络错误" };
            string oldPwd = Request.Form["oldpassword"];
            string newPwd = Request.Form["newpassword"];
            string rePwd = Request.Form["renewpassword"];
            if (string.IsNullOrWhiteSpace(oldPwd) || string.IsNullOrWhiteSpace(newPwd) || string.IsNullOrWhiteSpace(rePwd)) {
                state.Msg = "所填项不能为空!";
                return new JsonResult(state);
            }
            if (newPwd != rePwd) {
                state.Msg = "新密码和确认密码不一致!";
                return new JsonResult(state);
            }

            StateViewModel<string> userState = Tools.Auth.GetUserString(HttpContext);

            if (userState.Code != 200)
            {
                return new JsonResult(new { userState.Code, userState.Msg });
            }
            User userInfo = StringHelper.ParseFormJson<User>(userState.Data);

            if (userInfo.Password != CryptoHelper.EncryptText(oldPwd)) {
                state.Msg = "旧密码错误,请重新输入!";
                return new JsonResult(state);
            }

            IUserService user = new UserService();
            User model = new User() {
                Id= userInfo.Id,
                Password = CryptoHelper.EncryptText(newPwd),
                ManagerNum=userInfo.ManagerNum,
                CreateTime=userInfo.CreateTime
            };

            bool b =ValidateRegex.Password(newPwd);
            if (!b)
            {
                state.Msg = "密码长度在6-20之间，只能包含字符、数字和下划线。";
                return new JsonResult(state);
            }
            b = user.Save(model, true);
            if (b)
            {
                state.Code = 200;
                state.Msg = "修改密码成功";
            }
            else
            {
                state.Code = 300;
                state.Msg = "修改密码失败";
            }
            return new JsonResult(state);
        }
        #endregion

        #region 个人信息修改
        public JsonResult ChangeUserInfo(User model,UserInfo userInfo)
        {
            StateViewModel<string> userState = Tools.Auth.GetUserString(HttpContext);
            if (userState.Code != 200)
            {
                return new JsonResult(new { userState.Code, userState.Msg });
            }
            User userI = StringHelper.ParseFormJson<User>(userState.Data);

            StateViewModel<object> state = new StateViewModel<object>() { Code=400,Msg="网络错误!"};
            IUserService user = new UserService();
            
            if (!string.IsNullOrWhiteSpace(userInfo.Email))
            {
                if (!ValidateRegex.Email(userInfo.Email))
                {
                    state.Msg = "邮箱格式不正确!";
                    return new JsonResult(state);
                }
            }
            if (!string.IsNullOrWhiteSpace(userInfo.Phone))
            {
                if (!ValidateRegex.Phone(userInfo.Phone))
                {
                    state.Msg = "电话号码不正确!";
                    return new JsonResult(state);
                }
            }
            model.Username = userI.Username;
            model.CreateTime = userI.CreateTime;
            model.Id = userI.Id;
            userInfo.Id = userI.Id;
            userInfo.CreateTime = userI.CreateTime;
            model.ManagerNum = 1;
            int u = user.Edit(model, userInfo, out string msg);
            state.Code = u;
            state.Msg = msg;

            if (u == 200) {
                state.Data = new
                {
                    //user.Id, 不保存id
                    model.Nickname,
                    model.Username,
                    userInfo.FaceImg,
                    userInfo.Sex,
                    userInfo.Phone,
                    userInfo.Email,
                    model.CreateTime
                };

            }

            return new JsonResult(state);
        }
        #endregion
    }
}