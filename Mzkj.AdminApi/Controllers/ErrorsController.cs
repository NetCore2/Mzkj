﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Mzkj.AdminApi.Controllers
{
    [Produces("application/json")]
    public class ErrorsController : Controller
    {
        public JsonResult ECode()
        {
            string msg = Request.Query["Msg"];
            return new JsonResult(new { Code = 300, Msg = msg });
        }
    }
}