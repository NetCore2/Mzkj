﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Mzkj.AdminApi.Models;
using Mzkj.AdminApi.ViewModels;
using Mzkj.BLL;
using Mzkj.Common.Cache;
using Mzkj.Common.Tools;
using Mzkj.IBLL;
using Mzkj.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Mzkj.AdminApi.Controllers
{
    /*JWT权限登录*/
    [Route("[controller]/[Action]")]
    [AllowAnonymous]
    public class AuthController:Controller
    {
        private JwtSetting _jwtSettings;
        public AuthController(IOptions<JwtSetting> _jwtSettingsAccesser) {
            _jwtSettings = _jwtSettingsAccesser.Value;
        }

        [HttpPost]
        public IActionResult CreateToken(LoginViewModel viewModel) {
            StateViewModel<StateToken> state = new StateViewModel<StateToken>() { Code = 400, Msg = "网络错误" } ;
            if (ModelState.IsValid)
            {
                state.Code = 300;

                string code = CacheHelper.GetCache("LoginCode");
                //验证成功清除验证嘛
                CacheHelper.ClearCache("LoginCode");

                if (string.IsNullOrEmpty(code))
                {
                    state.Msg = "验证码错误!";
                }
                else {
                    if (viewModel.Code.ToLower() != code.ToLower())//不区分大小写
                    {
                        state.Msg = "验证码错误。";
                    }
                    else { 
                        IUserService userService = new UserService();
                        var user = userService.First(
                            it => it.Username == viewModel.UserName && it.Password == CryptoHelper.EncryptText(viewModel.Password) && it.Delflag == 0 && it.ManagerNum == 1 && it.Status == 1
                            );
                        if (user != null)
                        {
                            //登录成功记录到Redis+Cookies
                            string userLoginId = Guid.NewGuid().ToString();
                            CacheHelper.AddCache(userLoginId, StringHelper.GetJson<User>(user), _jwtSettings.Minutes);

                            //不用cookie保存
                            //string LoginIdGuid = Guid.NewGuid().ToString();
                            CookiesHelper.Add("userLoginId", userLoginId, HttpContext);
                            //CacheHelper.AddCache("LoginId_"+ LoginIdGuid, userLoginId, _jwtSettings.Minutes);

                            //生成JWT Token
                            var claims = new Claim[]
                            {
                                new Claim(ClaimTypes.Name,user.Username)
                                //,new Claim(ClaimTypes.Role,"admin")
                            };
                            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtSettings.SecretKey));
                            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                            var token = new JwtSecurityToken(
                                _jwtSettings.Issuer,
                                _jwtSettings.Audience,
                                claims,
                                DateTime.Now,
                                DateTime.Now.AddMinutes(_jwtSettings.Minutes),
                                creds
                            );

                            state.Code = 200;
                            state.Msg = "登录成功,正在跳转到系统首页!";
                            state.Data = new StateToken() {
                                access_token = new JwtSecurityTokenHandler().WriteToken(token),
                                expires_in = _jwtSettings.Minutes * 60,
                                timestamp = SystemHelper.DateTimeToUnixTimestamp(DateTime.Now)
                            };

                            CacheHelper.AddCache("token", state.Data.access_token, _jwtSettings.Minutes);

                            return Ok(state);
                        }
                        else { 
                            state.Msg = "用户名或密码错误!";
                        }
                    }
                }
                return Ok(state);
            }
            state.Msg = "";
            foreach (var s in ModelState.Values)
            {
                foreach (var p in s.Errors)
                {
                    state.Msg += p.ErrorMessage + ",";
                }
            }
            return BadRequest(state);
        }

        public IActionResult GetToken()
        {
            StateViewModel<StateToken> state = new StateViewModel<StateToken>() { Code =300, Msg = "未获取到token" };
            //获取Guid
            string userGuid = CookiesHelper.Get("userLoginId",HttpContext);
            //获取内存得token
            string token = CacheHelper.GetCache("token");
            if (!string.IsNullOrEmpty(token)) {
                state.Code = 200;
                state.Msg = "登录成功,正在跳转到系统首页!";
                state.Data = new StateToken() {
                    access_token = token,
                    expires_in = _jwtSettings.Minutes * 60,
                    timestamp = SystemHelper.DateTimeToUnixTimestamp(DateTime.Now)
                };
            }
            return Ok(state);
        }

        public JsonResult Logout()
        {
            //string userGuid = CacheHelper.GetCache("LoginId");
            string token = CacheHelper.GetCache("token");
            //删除redis key
            string userGuid = CookiesHelper.Get("userLoginId", HttpContext);
            //删除redis key
            CacheHelper.ClearCache(userGuid);
            CookiesHelper.Remove("userLoginId", HttpContext);
            CacheHelper.ClearCache(token);

            return new JsonResult(new{ Code=200,Msg="退出成功"});
        }

    }
}
