﻿using Mzkj.BLL;
using Mzkj.IBLL;
using Mzkj.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mzkj.AdminApi.Tools
{
    public static class Menu
    {
        static readonly IActionInfoService action = new ActionInfoService();
        public static List<ActionInfo> GetMenuOne(int uid)
        {
            List<ActionInfo> menuOne = null;
            if (uid == 1)
            {
                menuOne=action.GetList(it => it.Pid == 0 && it.Ismenu == 1);
            }
            else
            {
                List<int> rids=Auth.RoleIds(uid);
                rids = Auth.RuleIds(rids);
                menuOne = action.GetList(it => rids.Contains(it.Id) && it.Ismenu==1 && it.Pid == 0);
            }

            return menuOne;
        }

        //多级菜单
        public static Dictionary<string, object> GetMenuSe(int uid)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            List<ActionInfo> allList = new List<ActionInfo>();
            if (uid == 1)
            {
                allList = action.GetList(it =>it.Ismenu == 1);
            }
            else
            {
                List<int> rids = Auth.RoleIds(uid);
                rids = Auth.RuleIds(rids);
                allList = action.GetList(it => rids.Contains(it.Id) && it.Ismenu == 1);
            }
            dic=Menutree(allList, 0);

            return dic;
        }


        public static Dictionary<string, object> Menutree(List<ActionInfo> tree, int pid = 0)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            foreach (var item in tree)
            {
                if (item.Pid == pid)
                {
                    dic.Add(item.Dirname, MenutreeChild(tree, item.Id));
                }
            }
            return dic;
        }

        public static List<object> MenutreeChild(List<ActionInfo> tree, int pid = 0)
        {
            List<object> list = new List<object>();
            foreach (var item in tree)
            {
                if (item.Pid == pid) { 
                    list.Add(new {
                        Title=item.ActionName,
                        Icon= "&#xe630;",
                        Href="page/"+item.Dirname,
                        Spread=false,
                        Children= MenutreeChild(tree,item.Id)
                    });
                }
            }
            return list;
        }
    }
}
