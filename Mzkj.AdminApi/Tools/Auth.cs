﻿using Microsoft.AspNetCore.Http;
using Mzkj.AdminApi.ViewModels;
using Mzkj.BLL;
using Mzkj.IBLL;
using Mzkj.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mzkj.AdminApi.Tools
{
    public static class Auth
    {
        private static readonly IUserRoleService userRole = new UserRoleService();
        private static readonly IRoleActionService roleAction = new RoleActionService();
        //查询登录用户的角色IDS
        public static List<int> RoleIds(int uid){
            List<UserRole> luserRole = userRole.GetDatas().Where(it => it.Uid == uid).ToList();
            List<int> rids = new List<int>();
            for (int i = 0; i<luserRole.Count; i++)
            {
                rids.Add(luserRole[i].Rid);
            }
            return rids;
        }

        //根据角色查询权限IDS
        public static List<int> RuleIds(List<int> rids)
        {
            //根据角色查询权限Id
            List<RoleAction> lroleAction = roleAction.GetDatas().Where(it => rids.Contains(it.Rid)).ToList();
            List<int> aids = new List<int>();
            for (int i = 0; i < lroleAction.Count; i++)
            {
                aids.Add(lroleAction[i].Aid);
            }
            return aids;
        }

        #region user信息字符串
        public static StateViewModel<string> GetUserString(HttpContext context)
        {
            string userGuid = Common.Cache.CookiesHelper.Get("userLoginId", context);
            if (userGuid == null)
            {
                return new StateViewModel<string>() { Code = 300, Msg = "请登陆后操作!" };
            }
            string userStr = Common.Cache.CacheHelper.GetCache(userGuid);
            if (userStr == null)
            {
                return new StateViewModel<string>() { Code = 300, Msg = "非法用户!" };
            }
            return new StateViewModel<string>() { Code = 200, Data = userStr };
        }
        #endregion
    }
}
