﻿using Mzkj.AdminApi.ViewModels;
using Mzkj.Models;
using System.Collections.Generic;

namespace Mzkj.AdminApi.Tools
{
    public static class Xtree
    {
        //获取json tree 格式如下        
        //递归
        public static List<TreeJsonViewModel> Ctree(List<ActionInfo> tree, int pid = 0, List<int> rule = null)
        {
            List<TreeJsonViewModel> arr = new List<TreeJsonViewModel>();
            foreach (var item in tree)
            {
                TreeJsonViewModel o = new TreeJsonViewModel();
                if (item.Pid == pid)
                {
                    o.Title = item.ActionName;
                    o.Value = item.Id.ToString();
                    if (rule != null && rule.Count > 0)
                    {
                        if (rule.Contains(item.Id) && GetChildIds(tree, item.Id).Count == 0)
                        {
                            o.Checked = true;
                        }
                    }
                    o.Pid = item.Pid;
                    o.Data = Ctree(tree, item.Id, rule);
                    arr.Add(o);
                }
            }
            return arr;
        }
        public static List<int> GetChildIds(List<ActionInfo> tree, int pid = 0)
        {
            List<int> arr = new List<int>();
            foreach (var item in tree)
            {
                if (item.Pid == pid)
                {
                    arr.Add(item.Id);
                    arr.AddRange(GetChildIds(tree, item.Id));
                }
            }
            return arr;
        }
    }
}
