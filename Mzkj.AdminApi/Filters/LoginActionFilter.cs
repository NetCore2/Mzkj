﻿using Microsoft.AspNetCore.Mvc.Filters;
using Mzkj.Common;
using Mzkj.Common.Cache;
using Mzkj.Common.Tools;
using Mzkj.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mzkj.AdminApi.Filters
{
    public class LoginActionFilter: ActionFilterAttribute
    {
        public bool IsCheckLogin { get; set; }
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            base.OnActionExecuting(context);

            if (IsCheckLogin)
            {
                //使用Cookies+redis
                context.HttpContext.Request.Cookies.TryGetValue("userLoginId", out string userGuid);
                if (userGuid == null)
                {
                    context.HttpContext.Response.Redirect("/Admin/Login");
                    return;
                }

                string user = CacheHelper.GetCache(userGuid);
                if (user == null)
                {
                    //用户长时间不操作超时
                    context.HttpContext.Response.Redirect("/Admin/Login");
                    return;
                }
                User userInfo =StringHelper.ParseFormJson<User>(user);
                //滑动窗口机制 使用中不停更新缓存时间
                CacheHelper.SetCache(userGuid, user, Config.minutes);

            }
        }
    }
}
