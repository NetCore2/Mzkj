﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mzkj.AdminApi.ViewModels
{
    public class StateViewModel<T>
    {
        /// <summary>
        /// Code:
        /// 200   正常状态
        /// 300   错误状态
        /// 400   网络错误
        /// </summary>
        public int Code { set; get; }
        public string Msg { set; get; }
        public T Data { set; get; }
        /// <summary>
        /// 返回分页数据使用
        /// </summary>
        public int Total { set; get; }
    }
}
