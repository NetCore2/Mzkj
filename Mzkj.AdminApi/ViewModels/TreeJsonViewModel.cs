﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mzkj.AdminApi.ViewModels
{
    public class TreeJsonViewModel
    {
        public string Title { get; set; }
        public string Value { get; set; }
        public bool Checked { get; set; } = false;
        public bool Disabled { get; set; } = false;
        public int? Pid { get; set; }
        public List<TreeJsonViewModel> Data { get; set; }
    }

}
