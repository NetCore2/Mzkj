﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mzkj.AdminApi.ViewModels
{
    public class StateToken
    {
        public string access_token { get; set; }
        public int expires_in { get; set; }
        public long timestamp { set; get; }
    }
}
