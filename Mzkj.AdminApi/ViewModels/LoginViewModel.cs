﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Mzkj.AdminApi.ViewModels
{
    public class LoginViewModel
    {
        private const string V = "不能为空";

        [Required(ErrorMessage = "用户名"+V)]
        public string UserName { get; set; }

        [Required(ErrorMessage = "密码"+V)]
        public string Password { get; set; }
        public string Code { get; set; }
    }
}
