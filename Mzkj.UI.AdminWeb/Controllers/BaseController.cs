﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Mzkj.Common.Tools;
using Mzkj.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mzkj.UI.AdminWeb.Controllers
{
    public abstract class BaseController:Controller
    {
        public bool IsCheckLogin = true;

        public User LoginUser { get; set; }
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            base.OnActionExecuting(context);
            if (IsCheckLogin)
            {
                //使用Cookies+redis
                Request.Cookies.TryGetValue("userLoginId", out string userGuid);
                if (userGuid == null) {
                    context.HttpContext.Response.Redirect("/Admin/Login");
                    return;
                }
                string user=Common.Cache.CacheHelper.GetCache(userGuid);
                if (user == null)
                {
                    //用户长时间不操作超时
                    context.HttpContext.Response.Redirect("/Admin/Login");
                    return;
                }
                User userInfo = Common.Tools.StringHelper.ParseFormJson<User>(user);
                LoginUser = userInfo;
                //滑动窗口机制
                Common.Cache.CacheHelper.SetCache(userGuid, user, Common.Config.minutes);

                //使用 Session
                /*if (context.HttpContext.Session.GetString("User") == null)
                {
                    context.HttpContext.Response.Redirect("/Admin/Login");
                }else {
                    //LoginUser = JsonConvert.DeserializeObject<User>(context.HttpContext.Session.GetString("User"));
                    LoginUser = StringHelper.ParseFormJson<User>(context.HttpContext.Session.GetString("User"));
                }*/
            }
        }
    }
}
