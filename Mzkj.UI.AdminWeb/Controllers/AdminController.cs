﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Mzkj.BLL;
using Mzkj.Common;
using Mzkj.Common.Cache;
using Mzkj.Common.Tools;
using Mzkj.IBLL;
using Mzkj.Models;
using Mzkj.UI.AdminWeb.Models;

namespace Mzkj.UI.AdminWeb.Controllers
{
    public class AdminController : Controller
    {
        [LoginActionFilter(IsCheckLogin =true)]
        public IActionResult Index()
        {
            Request.Cookies.TryGetValue("userLoginId", out string userGuid);
            ViewData["cache"] = CacheHelper.GetCache(userGuid);
            return View();
            /*ViewData["cache"] = CacheHelper.GetCache("key");

            ViewData["user"] = HttpContext.Session.GetString("User");
            ViewData["code"] = HttpContext.Session.GetString("LoginCode");

            IUserService us = new UserService();
            List<User> uus=us.GetList();
            ViewData["uus"] = uus;
            return View(uus);*/
            //return Content(ByteConvertHelper.BytesToObject(user));
            //return new JsonResult(ByteConvertHelper.BytesToObject(user));
        }


        public IActionResult Login()
        {
            /*CacheHelper.AddCache("key", "123123");
            ViewData["ip"]=SystemHelper.GetIP(HttpContext);*/
            return View();
        }

        [HttpPost]
        public IActionResult Login(LoginViewModel model)
        {
            if (ModelState.IsValid) {
                string code = HttpContext.Session.GetString("LoginCode");
                //验证成功清除验证嘛
                HttpContext.Session.Remove("LoginCode");

                if (string.IsNullOrEmpty(code)) {
                    ModelState.AddModelError("", "验证码错误。");
                    return View(model);
                }
                if (model.VCode == code) {

                    //和数据库对比
                    IUserService userService = new UserService();
                    var user = userService.First(it => it.Username == model.Username && it.Password == model.Password);
                    if (user != null) {
                        /*//使用 Session
                        HttpContext.Session.SetString("User",StringHelper.GetJson<User>(user));
                        */


                        //登录成功记录到Redis+Cookies
                        string userLoginId = Guid.NewGuid().ToString();
                        CacheHelper.AddCache(userLoginId, StringHelper.GetJson<User>(user), Config.minutes);
                        Response.Cookies.Append("userLoginId",userLoginId);

                        //跳转到系统首页
                        return RedirectToAction("Index", "Home");
                    }
                    return Content("没有查询到数据");
                }
                ModelState.AddModelError("", "验证码错误。");
                return View(model);
            }
            ModelState.AddModelError("", "用户名或密码错误。");
            return View(model);
        }

        //验证码
        public IActionResult Vcode()
        {
            string code = "";
            System.IO.MemoryStream ms = (new ValidateCodeTools()).Create(out code);
            HttpContext.Session.SetString("LoginCode", code);
            Response.Body.Dispose();
            return File(ms.ToArray(), @"image/png");
        }
    }
}