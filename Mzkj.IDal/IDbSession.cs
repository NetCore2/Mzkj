﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mzkj.IDal
{
    public interface IDbSession
    {
        IUserDal UserDal { get; }
        IUserInfoDal UserInfoDal { get; }
        /// <summary>
        /// 权限
        /// </summary>
        IActionInfoDal ActionInfoDal { get; }
        IRoleDal RoleDal { get; }
        IUserRoleDal UserRoleDal { get; }
        IRoleActionDal RoleActionDal { get; }
    }
}
