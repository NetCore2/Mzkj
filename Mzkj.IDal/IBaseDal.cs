﻿using System;
using SqlSugar;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Mzkj.IDal
{
    public interface IBaseDal<T> where T:class, new()
    {
        #region 简单查询
        /// <summary>
        /// 根据主键查询
        /// </summary>
        /// <returns>返回单个Model对象</returns>
        T Find(dynamic id);
        List<T> Find(dynamic[] ids);

        /// <summary>
        /// 查询第一个
        /// </summary>
        /// <returns>返回单个Model对象</returns>
        T First();
        //T First(string orderField);
        /// <summary>
        /// 带Lambda查询第一个
        /// </summary>
        /// <param name="whereLambda">拉姆达表达式</param>
        /// <returns></returns>
        T First(Expression<Func<T, bool>> whereLambda);
        T First(Expression<Func<T, bool>> whereLambda, string orderField);
        #endregion

        #region 查询
        #region 查询返回全部List数据 带排序
        /// <summary>
        /// 查询返回全部List数据
        /// </summary>
        /// <param name="orderField">排序字符串(默认:"Id Asc")</param>
        /// <returns>返回List集合</returns>
        List<T> GetList();
        //List<T> GetList(string orderField);
        #endregion

        #region 自定义条件查询返回List对象 带排序
        /// <summary>
        /// 自定义条件查询返回List对象
        /// </summary>
        /// <param name="whereLambda">拉姆达查询语句如(it=>id.Id>1)</param>
        /// <param name="orderField">排序字符串(默认:"Id Asc")</param>
        /// <returns>ISugarQueryable</returns>
        List<T> GetList(Expression<Func<T, bool>> whereLambda);
        List<T> GetList(Expression<Func<T, bool>> whereLambda, string orderField);
        #endregion

        #region 自定义分页查询返回List 带排序
        /// <summary>
        /// 自定义条件查询数据
        /// </summary>
        /// <param name="pageIndex">当前页码数</param>
        /// <param name="page">页码PageModel</param>
        /// <param name="whereLambda">拉姆达查询语句如(it=>id.Id>1)</param>
        /// <returns>返回List集合</returns>
        List<T> GetPageList(Expression<Func<T, bool>> whereLambda, int pageIndex = 1);

        List<T> GetPageList(Expression<Func<T, bool>> whereLambda, int pageIndex, string orderField);
        List<T> GetPageList(Expression<Func<T, bool>> whereLambda, int pageIndex, string orderField, PageModel page);
        #endregion

        #region 复杂的排序查询
        /// <summary>
        /// 查询返回全部ISugarQueryable对象
        /// </summary>
        /// <returns>ISugarQueryable</returns>
        ISugarQueryable<T> GetDatas();
        /// <summary>
        /// 自定义条件查询返回ISugarQueryable对象
        /// </summary>
        /// <param name="whereLambda">拉姆达查询语句如(it=>id.Id>1)</param>
        /// <returns>ISugarQueryable</returns>
        ISugarQueryable<T> GetDatas(Expression<Func<T, bool>> whereLambda);

        /// <summary>
        /// 分页查询默认1页
        /// </summary>
        /// <param name="pageIndex">页码参数</param>
        /// <returns></returns>
        ISugarQueryable<T> GetPageDatas(int pageIndex = 1);
        ISugarQueryable<T> GetPageDatas(int pageIndex, string orderField);
        ISugarQueryable<T> GetPageDatas(int pageIndex, string orderField, PageModel page);

        /// <summary>
        /// 自定义条件查询数据
        /// </summary>
        /// <param name="pageIndex">当前页码数</param>
        /// <param name="page">页码PageModel</param>
        /// <param name="whereLambda">拉姆达查询语句如(it=>id.Id>1)</param>
        /// <returns>返回List集合</returns>
        ISugarQueryable<T> GetPageDatas(Expression<Func<T, bool>> whereLambda, int pageIndex = 1);
        ISugarQueryable<T> GetPageDatas(Expression<Func<T, bool>> whereLambda, int pageIndex, string orderField);
        ISugarQueryable<T> GetPageDatas(Expression<Func<T, bool>> whereLambda, int pageIndex, string orderField, PageModel page);
        #endregion

        #region 查询是否存在数据 IsAny
        bool IsAny(Expression<Func<T, bool>> whereLambda);
        #endregion

        #endregion

        #region 插入
        /// <summary>
        /// 返回受影响的行数
        /// </summary>
        /// <param name="inObject">List,Dictionary,T对象,T[]数组</param>
        /// <returns></returns>
        IInsertable<T> Insert(dynamic inObject);
        IInsertable<T> Insert(dynamic inObject, Expression<Func<T, T>> fieldLambda);
        /// <summary>
        /// 返回布尔值,更新操作 主键必须有值
        /// </summary>
        /// <param name="inObject">List,Dictionary,T对象,T[]数组</param>
        /// <returns></returns>
        bool Add(dynamic inObject);
        #endregion

        #region 修改
        /// <summary>
        /// 返回布尔值,更新操作 主键必须有值
        /// </summary>
        /// <param name="inObject">List,Dictionary,T对象,T[]数组</param>
        /// <returns></returns>
        bool Save(dynamic updateObj);
        bool Save(T updateObj,bool field);

        /// <summary>
        /// 主键必须有值
        /// </summary>
        /// <param name="updateObj">List,Dictionary,T对象,T[]数组</param>
        /// <returns></returns>
        IUpdateable<T> Update(dynamic updateObj);

        /// <summary>
        /// 常用更新的模式
        /// </summary>
        /// <param name="columns">需要更新的拉姆达表达式(如:it => new Student() { Name = "a", CreateTime = DateTime.Now })</param>
        /// <param name="whereExpression">拉姆达表达式(如:it => it.Id == 11)按照该条件更新</param>
        /// <returns></returns>
        bool Update(Expression<Func<T, bool>> columns, Expression<Func<T, bool>> whereLambda);
        #endregion


        #region 删除
        /// <summary>
        /// 根据对象删除
        /// </summary>
        /// <param name="deleteObj">T对象</param>
        /// <returns>布尔值</returns>
        bool Delete(T deleteObj);
        /// <summary>
        /// 根据lambda删除
        /// </summary>
        /// <param name="whereLambda">拉姆达表达式</param>
        /// <returns>布尔值</returns>
        bool Delete(Expression<Func<T, bool>> whereLambda);

        /// <summary>
        /// 根据主键删除单个
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns>布尔值</returns>
        bool Del(dynamic id);
        /// <summary>
        /// 根据主键删除多个
        /// </summary>
        /// <param name="ids">主键数组</param>
        /// <returns>布尔值</returns>
        bool Del(dynamic[] ids);
        #endregion


        #region 聚合查询
        /// <summary>
        /// 返回数据总数
        /// </summary>
        /// <returns>整数</returns>
        int Count();
        /// <summary>
        /// 返回数据总数
        /// </summary>
        /// <param name="whereLambda">拉姆达表达式where</param>
        /// <returns>整数</returns>
        int Count(Expression<Func<T, bool>> whereLambda);
        #endregion

        IAdo Ado();

        /// <summary>
        /// 自定义查询
        /// </summary>
        /// <param name="u"></param>
        /// <param name="dic"></param>
        /// <returns></returns>
        ISugarQueryable<T> Search(ISugarQueryable<T> u, Dictionary<string, string> dic);
        List<T> Search(ISugarQueryable<T> u, int pageIndex, string orderField, PageModel page);
    }
}
