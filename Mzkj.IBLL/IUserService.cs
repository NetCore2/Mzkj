﻿using Mzkj.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mzkj.IBLL
{
    public interface IUserService:IBaseService<User>
    {
        bool SetRole(int userId, List<UserRole> roles);

        int Add(User model, UserInfo uinfo, out string msg);
        int Del(List<int> ids, out string msg);
        int Edit(User model, UserInfo uinfo, out string msg);
    }
}
