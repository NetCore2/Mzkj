﻿using Mzkj.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mzkj.IBLL
{
    public interface IRoleService : IBaseService<Role>
    {
        bool SetAction(int roleId, List<RoleAction> actions);
    }
}
