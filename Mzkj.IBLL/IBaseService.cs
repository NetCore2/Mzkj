﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using SqlSugar;

namespace Mzkj.IBLL
{
    public interface IBaseService<T> where T:class,new()
    {
        #region 简单查询
        T Find(dynamic id);
        T First();
        //T First(string orderField);
        T First(Expression<Func<T, bool>> whereLambda);
        T First(Expression<Func<T, bool>> whereLambda, string orderField);
        #endregion

        #region 查询
        #region 查询返回全部List数据 带排序
        List<T> GetList();
        //List<T> GetList(string orderField);
        #endregion

        #region 自定义条件查询返回List对象 带排序
        List<T> GetList(Expression<Func<T, bool>> whereLambda);
        List<T> GetList(Expression<Func<T, bool>> whereLambda, string orderField);
        #endregion

        #region 自定义分页查询返回List 带排序
        List<T> GetPageList(Expression<Func<T, bool>> whereLambda, int pageIndex = 1);
        List<T> GetPageList(Expression<Func<T, bool>> whereLambda, int pageIndex, string orderField);
        List<T> GetPageList(Expression<Func<T, bool>> whereLambda, int pageIndex, string orderField, PageModel page);
        #endregion

        #region 复杂的排序查询
        ISugarQueryable<T> GetDatas();
        ISugarQueryable<T> GetDatas(Expression<Func<T, bool>> whereLambda);
        ISugarQueryable<T> GetPageDatas(int pageIndex = 1);
        ISugarQueryable<T> GetPageDatas(int pageIndex, string orderField);
        ISugarQueryable<T> GetPageDatas(int pageIndex,
            string orderField,
            PageModel page);
        ISugarQueryable<T> GetPageDatas(
            Expression<Func<T, bool>> whereLambda,
            int pageIndex = 1);
        ISugarQueryable<T> GetPageDatas(
            Expression<Func<T, bool>> whereLambda,
            int pageIndex,
            string orderField);
        ISugarQueryable<T> GetPageDatas(
            Expression<Func<T, bool>> whereLambda,
            int pageIndex,
            string orderField,
            PageModel page);
        #endregion

        #region 查询是否存在数据 IsAny
        bool IsAny(Expression<Func<T, bool>> whereLambda);
        #endregion

        #endregion

        #region 插入
        IInsertable<T> Insert(dynamic inObject);
        IInsertable<T> Insert(dynamic inObject, Expression<Func<T, T>> fieldLambda);
        bool Add(dynamic inObject);
        #endregion

        #region 修改
        bool Save(dynamic updateObj);
        bool Save(T updateObj,bool field);
        IUpdateable<T> Update(dynamic updateObj);
        bool Update(Expression<Func<T, bool>> columns, Expression<Func<T, bool>> whereLambda);
        #endregion

        #region 删除
        bool Delete(T deleteObj);
        bool Delete(Expression<Func<T, bool>> whereLambda);
        bool Del(dynamic id);
        int Count();
        int Count(Expression<Func<T, bool>> whereLambda);
        #endregion

        IAdo Ado();

        #region 条件查询
        ISugarQueryable<T> Search(ISugarQueryable<T> u, Dictionary<string, string> dic);
        List<T> Search(ISugarQueryable<T> u, int pageIndex, string orderField, PageModel page);
        #endregion

    }
}
