﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.RegularExpressions;

namespace Mzkj.Common.Tools
{
    public static class ValidateRegex
    {
        public static bool Email(string email)
        {
            return Regex.IsMatch(email, @"^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+$");
        }

        public static bool TelPhone(string phone)
        {
            return Regex.IsMatch(phone, @"^(\d{3,4}-)?\d{7,8})$");
        }
        public static bool Phone(string phone)
        {
            return Regex.IsMatch(phone, @"^1[34578]\d{9}$");
        }
        //Model判断
        public static bool Password(string password)
        {
            return Regex.IsMatch(password, @"^\w{6,20}$");
        }
    }
    
}
