﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Mzkj.Common.Tools
{
    public static class UploadHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="files">上传得Files</param>
        /// <param name="curName">指定上传得二级路径</param>
        /// <param name="sizeM"></param>
        /// <returns></returns>
        public static List<StateFace> UploadFile(IFormFileCollection files, int sizeM, string curName="default", string typeName = "image/jpg,image/jpeg,image/png")
        {
            List<StateFace> list = new List<StateFace>();
            string path = string.Empty;     //文件全路径
            string filePath = string.Empty; //文件夹路径
            string fileExt = string.Empty;   //不带点后缀名

            //返回数据
            string msg = string.Empty;
            
            string[] TName = typeName.Split(',');

            long size = files.Sum(f => f.Length);
            //string webRootPath = environment.WebRootPath;
            string webRootPath = Path.Combine(Environment.CurrentDirectory,"wwwroot");
            Random rd = new Random();

            foreach (var formFile in files)
            {
                if (formFile.Length > 0)
                {
                    string src = Path.Combine("upload", curName, DateTime.Now.ToString("yyyyMMdd"));
                    filePath = Path.Combine(webRootPath, src);
                    fileExt = SystemHelper.GetFileExt(formFile.FileName);
                    string fileName = DateTime.Now.ToString("HHmmss") + rd.Next(1000, 9999).ToString() + "." + fileExt;
                    path = Path.Combine(filePath, fileName);

                    if(!TName.Contains(formFile.ContentType)){
                        msg = "上传的文件类型错误!";
                        list.Add(new StateFace() { Title = msg });
                        continue;
                    }

                    long fileSize = formFile.Length; //获得文件大小，以字节为单位
                    if (fileSize >= sizeM * 1024 * 1024) {
                        msg = "上传的图片超出大小,请重新上传!";
                        list.Add(new StateFace() { Title = msg });
                        continue;
                    }
                    if (!Directory.Exists(filePath))
                    {
                        Directory.CreateDirectory(filePath);
                    }
                    using (var stream = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite))
                    {
                        formFile.CopyToAsync(stream);
                    }
                    list.Add(new StateFace() { Src = Path.Combine(src, fileName),Title= formFile.FileName });
                }

            }

            return list;
        }

        public static StateFace UploadOne(IFormFileCollection files, int sizeM, string curName = "default", string typeName = "image/jpg,image/jpeg,image/png")
        {
            StateFace state = new StateFace();
            string path = string.Empty;     //文件全路径
            string filePath = string.Empty; //文件夹路径
            string fileExt = string.Empty;   //不带点后缀名

            //返回数据
            string msg = string.Empty;

            string[] TName = typeName.Split(',');

            long size = files.Sum(f => f.Length);
            //string webRootPath = environment.WebRootPath;
            string webRootPath = Path.Combine(Environment.CurrentDirectory, "wwwroot");
            Random rd = new Random();

            if (files.Count != 1) {
                msg = "只能上传一个文件!";
                state.Title = msg;
                return state;
            }

            foreach (var formFile in files)
            {
                if (formFile.Length > 0)
                {
                    string src = Path.Combine("upload", curName, DateTime.Now.ToString("yyyyMMdd"));
                    filePath = Path.Combine(webRootPath, src);
                    fileExt = SystemHelper.GetFileExt(formFile.FileName);
                    string fileName = DateTime.Now.ToString("HHmmss") + rd.Next(1000, 9999).ToString() + "." + fileExt;
                    path = Path.Combine(filePath, fileName);

                    if (!TName.Contains(formFile.ContentType))
                    {
                        msg = "上传的文件类型错误,不支持该格式!";
                        state.Title = msg;
                        continue;
                    }

                    long fileSize = formFile.Length; //获得文件大小，以字节为单位
                    if (fileSize >= sizeM * 1024 * 1024)
                    {
                        msg = "上传的图片超出大小,请重新上传!";
                        state.Title = msg;
                        continue;
                    }
                    if (!Directory.Exists(filePath))
                    {
                        Directory.CreateDirectory(filePath);
                    }
                    using (var stream = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite))
                    {
                        formFile.CopyToAsync(stream);
                    }
                    state.Title = msg;
                    state.Src = Path.Combine(src, fileName);
                }

            }

            return state;
        }
    }

    public class StateFace
    {
        public string Src { set; get; }
        public string Title { set; get; }
    }
}
