﻿using Mzkj.Common.Model.Tree;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace Mzkj.Common.Tools
{
    public static class ModelHelper
    {
        /// <summary>
        /// 无限极分类
        /// </summary>
        /// <param name="list"></param>
        /// <param name="pid"></param>
        /// <param name="level"></param>
        /// <returns></returns>
        public static List<CateTree> Ctree(List<CateTree> list,int pid=0,int level=0)
        {
            //List<CateTree> arr = new List<CateTree>();

            List<CateTree> ls = list.Where(m=>m.Pid==pid).ToList();
            CateTree[] arr = ls.ToArray();
            if (arr.Length>0)
            {
                for (int i = 0; i < arr.Length; i++)
                {
                    arr[i].Level = level + 1;
                    //递归调用创建子节点
                    arr[i].Children = Ctree(list, arr[i].Id, level + 1);
                }
                return arr.ToList();
            }
            else {
                return null;
            }
        }

        public static List<int> ChildIds(List<CateTree> list, int pid = 0)
        {
            List<CateTree> ls = list.Where(m => m.Pid == pid).ToList();
            List<int> larr = new List<int>();
            int[] arr = larr.ToArray();
            if (ls.Count > 0) { 
                for (int i = 0; i < ls.Count; i++)
                {
                    //arr[i]=ls[i].Id;
                    larr.Add(ls[i].Id);
                    larr=larr.Union(ChildIds(list, ls[i].Id)).ToList<int>();
                    //Array.Copy(arr, ChildIds(list, ls[i].Id),arr.Length);
                }
            }
            return larr;
        }
        
    }
}
