﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Mzkj.Common.Logs
{
    //委托方式 public delegate void WriteLogDel(string str);

    public class LogHelper
    {
        public static ConcurrentQueue<string> ExceptionStringQueue = new ConcurrentQueue<string>();

        //定义list 观察者

        public static List<ILogWriter> LogWriterList = new List<ILogWriter>();

        static LogHelper(){

            LogWriterList.Add(new Log4NetWriter());

            //把从队列中获取的错误消息写入到 日志文件中去
            ThreadPool.QueueUserWorkItem(o =>
            {
                lock (ExceptionStringQueue)
                {
                    //出队列一个异常信息
                    ExceptionStringQueue.TryDequeue(out string str);

                    //把异常信息写到 日志文件里面去
                    //变化:有可能写入到数据库
                    //观察者模式
                    foreach (var logWriter in LogWriterList)
                    {
                        logWriter.WriteLogInfo(str);
                    }
                }
            });
        }
        public static void WriteLog(string exceptionText)
        {
            lock (ExceptionStringQueue)
            {
                //入队列一个异常信息
                ExceptionStringQueue.Enqueue(exceptionText);
            }
        }
    }
}
