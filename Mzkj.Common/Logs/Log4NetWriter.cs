﻿using log4net;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mzkj.Common.Logs
{
    public class Log4NetWriter : ILogWriter
    {
        public void WriteLogInfo(string str)
        {
            ILog log = LogManager.GetLogger("NETCoreLogger","错误日志");
            log.Error(str);
        }
    }
}
