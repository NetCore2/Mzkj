﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mzkj.Common
{
    public class Config
    {
        public static ConnectionConfig SqlSugarConfig = new ConnectionConfig()
        {
            ConnectionString = "Database=vashi_core2.0;Data Source=127.0.0.1;User Id=root;Password=root;pooling=false;CharSet=utf8;port=3306",
            DbType = DbType.MySql,
            IsAutoCloseConnection = true
        };
        // redis 设置
        public static string RedisConfig= "127.0.0.1" + ":" + "6379";
        // redis保存时间设置 分钟
        public static int minutes = 30;


        // 默认页码设置
        public static PageModel pageCommon = new PageModel()
        {
            PageCount = 0,
            PageIndex = 1,
            PageSize = 20
        };

        //设置别名表
        //key类名 value表名
        public static IEnumerable<MappingTable> MpList = new List<MappingTable>(){
            new MappingTable(){ EntityName="User", DbTableName="lc_users"},
            new MappingTable(){ EntityName="UserInfo", DbTableName="lc_users_info"},
            new MappingTable(){ EntityName="UserRole", DbTableName="lc_user_role"},
            new MappingTable(){ EntityName="Role", DbTableName="lc_role"},
            new MappingTable(){ EntityName="RoleAction", DbTableName="lc_role_action"},
            new MappingTable(){ EntityName="ActionInfo", DbTableName="lc_action"}
        };
        public static IEnumerable<MappingColumn> MpColumnList = new List<MappingColumn>(){
            new MappingColumn(){ EntityName="id", DbColumnName="Id"}
        };

        public static Dictionary<string, string> Crypto = new Dictionary<string, string> {
            { "key","PD3hqfxI7S5oXUgJdpzHKyOY8aErbWNeiMRBwktvcnjZLVCAF2suQ6T09G41ml" },
            { "md5","LUCK48_COM" }
        };
    }
    
}
