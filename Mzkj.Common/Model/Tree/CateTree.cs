﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mzkj.Common.Model.Tree
{
    public class CateTree
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public byte Ismenu { get; set; }
        public byte? Status { get; set; }
        public byte? Delflag { get; set; }
        public int? Pid { get; set; } = 0;
        public int Level { get; set; }
        public long? CreateTime { get; set; }
        public List<CateTree> Children { get; set; }
    }
}
