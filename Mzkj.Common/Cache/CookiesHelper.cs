﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mzkj.Common.Cache
{
    public static class CookiesHelper
    {
        public static void Add(string key, string value,HttpContext context)
        {
            context.Response.Cookies.Append(key, value);
        }

        public static void Remove(string key, HttpContext context)
        {
            context.Response.Cookies.Delete(key);
        }

        public static string Get(string key, HttpContext context)
        {
            context.Request.Cookies.TryGetValue(key, out string userGuid);
            
            return userGuid;
        }
    }
}
