﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mzkj.Common.Cache
{
    public class CacheHelper
    {
        public static ICacheWriter CacheWriter { get; set; }
        private static readonly object Locker = new object();

        static CacheHelper()
        {
            if (CacheWriter == null)
            {
                lock (Locker)
                {
                    if (CacheWriter == null)
                    {
                        CacheWriter = new RedisWriter();
                    }
                }
            }
        }

        public static void AddCache(string key, string value,int expDate) {
            CacheWriter.AddCache(key, value, expDate);
        }
        public static void AddCache(string key, string value)
        {
             CacheWriter.AddCache(key, value);
        }
        public static string GetCache(string key)
        {
            return CacheWriter.GetCache(key);
        }

        public static void SetCache(string key, string value, int expDate)
        {
            CacheWriter.SetCache(key, value, expDate);
        }
        public static void SetCache(string key, string value)
        {
            CacheWriter.SetCache(key, value);
        }

        public static void ClearCache(string key)
        {
            CacheWriter.ClearCache(key);
        }
    }
}
