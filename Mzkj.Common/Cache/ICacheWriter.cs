﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mzkj.Common.Cache
{
    public interface ICacheWriter
    {
        void AddCache(string key, string value, int expDate);
        void AddCache(string key, string value);
        string GetCache(string key);

        void SetCache(string key, string value, int expDate);
        void SetCache(string key, string value);
        void ClearCache(string key);
    }
}
