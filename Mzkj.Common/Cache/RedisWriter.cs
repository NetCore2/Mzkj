﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mzkj.Common.Cache
{
    public class RedisWriter : ICacheWriter
    {
        public static IDatabase RedisDB;
        public RedisWriter()
        {
            RedisDB = RedisHepler.RedisConn.GetDatabase();
        }

        


        public void AddCache(string key, string value, int expDate)
        {
            RedisDB.StringSet(key, value, TimeSpan.FromMinutes(expDate));
        }

        public void AddCache(string key, string value)
        {
            RedisDB.StringSet(key, value);
        }

        public string GetCache(string key)
        {
            return RedisDB.StringGet(key);
        }

        public void SetCache(string key, string value, int expDate)
        {
            RedisDB.StringSet(key, value, TimeSpan.FromMinutes(expDate));
        }

        public void SetCache(string key, string value)
        {
            RedisDB.StringSet(key, value);
        }

        public void ClearCache(string key)
        {
            if (key != null) { 
                RedisDB.KeyDelete(key);
            }
        }
    }
}
