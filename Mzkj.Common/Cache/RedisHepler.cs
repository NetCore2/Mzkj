﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mzkj.Common.Cache
{
    public class RedisHepler
    {
        private static ConfigurationOptions configurationOptions = ConfigurationOptions.Parse(Config.RedisConfig);
        private static readonly object Locker = new object();
        private static ConnectionMultiplexer redisConn;

        /// <summary>
        /// 单例获取
        /// </summary>
        public static ConnectionMultiplexer RedisConn
        {
            get
            {
                if (redisConn == null)
                {
                    lock (Locker)
                    {
                        if (redisConn == null || !redisConn.IsConnected)
                        {
                            redisConn = ConnectionMultiplexer.Connect(configurationOptions);
                        }
                    }
                }
                return redisConn;
            }
        }
    }
}
