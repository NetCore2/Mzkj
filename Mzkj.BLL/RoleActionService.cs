﻿using Mzkj.IBLL;
using Mzkj.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mzkj.BLL
{
    public class RoleActionService : BaseService<RoleAction>, IRoleActionService
    {
        public override void SetCurrentDal()
        {
            CurrentDal = DbSession.RoleActionDal;
        }
    }
}
