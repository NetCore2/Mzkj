﻿using Mzkj.IBLL;
using Mzkj.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Mzkj.Common.Tools;

namespace Mzkj.BLL
{
    public class UserService : BaseService<User>,IUserService
    {
        UserInfoService userInfo = new UserInfoService();
        #region 添加用户和Info表
        /// <summary>
        /// 添加user和userInfo表的数据
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int Add(User model,UserInfo uinfo,out string msg) {
            try
            {
                CurrentDal.Ado().BeginTran();
                model.Password = CryptoHelper.EncryptText(model.Password);
                User user = First(it => it.Username == model.Username);
                if (user != null) {
                    msg = "用户名重复!";
                    return 300;
                }
                int uid =Insert(model).ExecuteReturnIdentity();
                uinfo.Uid = uid;
                //UserInfoService userInfo = new UserInfoService();
                bool b=userInfo.Add(uinfo);
                if (b)
                {
                    CurrentDal.Ado().CommitTran();
                    msg = "添加成功";
                    return 200;
                }
                else
                {
                    msg = "添加失败";
                    return 300;
                }
            }
            catch (Exception)
            {
                CurrentDal.Ado().RollbackTran();
                msg = "添加失败,未知错误";
                return 400;
            }
        }
        #endregion

        #region 修改用户和Info表
        /// <summary>
        /// 添加user和userInfo表的数据
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int Edit(User model, UserInfo uinfo, out string msg)
        {
            try
            {
                CurrentDal.Ado().BeginTran();                
                User user = First(it => it.Username == model.Username && it.Id!=model.Id);
                if (user != null)
                {
                    msg = "用户名重复!";
                    return 300;
                }
                bool a = false;
                if (!string.IsNullOrEmpty(model.Password))
                {
                    model.Password = CryptoHelper.EncryptText(model.Password);
                    a = Save(model,true);
                }
                else {
                    a = Save(model, true);
                }
                if (!a) {
                    msg = "修改失败";
                    return 300;
                }
                uinfo.Uid = model.Id;
                //UserInfoService userInfo = new UserInfoService();
                bool b = userInfo.Save(uinfo, true);
                if (b)
                {
                    CurrentDal.Ado().CommitTran();
                    msg = "修改成功";
                    return 200;
                }
                else
                {
                    msg = "修改失败";
                    return 300;
                }
            }
            catch (Exception)
            {
                CurrentDal.Ado().RollbackTran();
                msg = "修改失败,未知错误";
                return 400;
            }
        }
        #endregion

        public int Del(List<int> ids, out string msg) {
            try
            {
                CurrentDal.Ado().BeginTran();
                
                bool a = Del(ids);
                if (!a)
                {
                    msg = "删除失败";
                    return 300;
                }
                
                bool b = userInfo.Delete(it=>ids.Contains(it.Uid));
                if (b)
                {
                    CurrentDal.Ado().CommitTran();
                    msg = "删除成功";
                    return 200;
                }
                else
                {
                    msg = "删除失败";
                    return 300;
                }
            }
            catch (Exception)
            {
                CurrentDal.Ado().RollbackTran();
                msg = "删除失败,未知错误";
                return 400;
            }
        }

        /// <summary>
        /// 设置权限
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public bool SetRole(int userId, List<UserRole> roles)
        {
            //RoleService role = new RoleService();
            UserRoleService userRole = new UserRoleService();
            //var user=Find(userId);

            bool b=userRole.Delete(r => r.Uid == userId);
            if (roles.Count > 0)
            {
                b = userRole.Insert(roles.ToArray()).ExecuteCommand() > 0;
            }

            return b;
        }

        public override void SetCurrentDal()
        {
            CurrentDal=DbSession.UserDal;
        }
    }
}
