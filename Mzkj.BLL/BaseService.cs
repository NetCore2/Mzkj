﻿using Mzkj.DALFactory;
using Mzkj.IDal;
using Mzkj.SqlSugarDAL;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Mzkj.BLL
{
    public abstract class BaseService<T> where T:class,new()
    {
        public string orderField = "Id Asc";//默认排序"Id Asc"
        public IBaseDal<T> CurrentDal { get; set; }
        public IDbSession DbSession
        {
            get { return DbSessionFactory.GetDbSession(); }
        }
        public BaseService()
        {
            SetCurrentDal();
        }
        //通过抽象方法必须在实例的时候实现 获取DAL
        public abstract void SetCurrentDal();

        public PageModel Pages
        {
            get { return DbFactory.Page(); }
        }

        #region 简单查询
        /// <summary>
        /// 根据主键查询
        /// </summary>
        /// <returns>返回单个Model对象</returns>
        public T Find(dynamic id)
        {
            return CurrentDal.Find(id);
        }

        /// <summary>
        /// 查询第一个
        /// </summary>
        /// <returns>返回单个Model对象</returns>
        public T First()
        {
            return First();
        }
        //public T First(string orderField)
        //{
        //    return CurrentDal.First(orderField);
        //}
        /// <summary>
        /// 带Lambda查询第一个
        /// </summary>
        /// <param name="whereLambda">拉姆达表达式</param>
        /// <returns></returns>
        public T First(Expression<Func<T, bool>> whereLambda)
        {
            return First(whereLambda, orderField);
        }
        public T First(Expression<Func<T, bool>> whereLambda, string orderField)
        {
            return CurrentDal.First(whereLambda, orderField);
        }
        #endregion

        #region 查询
        #region 查询返回全部List数据 带排序
        /// <summary>
        /// 查询返回全部List数据
        /// </summary>
        /// <param name="orderField">排序字符串(默认:"Id Asc")</param>
        /// <returns>返回List集合</returns>
        public List<T> GetList()
        {
            var getAll = CurrentDal.GetList();
            return getAll;
        }
        //public List<T> GetList(string orderField)
        //{
        //    var getAll = CurrentDal.GetList(orderField);
        //    return getAll;
        //}
        #endregion

        #region 自定义条件查询返回List对象 带排序
        /// <summary>
        /// 自定义条件查询返回List对象
        /// </summary>
        /// <param name="whereLambda">拉姆达查询语句如(it=>id.Id>1)</param>
        /// <param name="orderField">排序字符串(默认:"Id Asc")</param>
        /// <returns>ISugarQueryable</returns>
        public List<T> GetList(Expression<Func<T, bool>> whereLambda)
        {
            return GetList(whereLambda, orderField);
        }
        public List<T> GetList(Expression<Func<T, bool>> whereLambda, string orderField)
        {
            return CurrentDal.GetList(whereLambda, orderField);
        }
        #endregion

        #region 自定义分页查询返回List 带排序
        /// <summary>
        /// 自定义条件查询数据
        /// </summary>
        /// <param name="pageIndex">当前页码数</param>
        /// <param name="page">页码PageModel</param>
        /// <param name="whereLambda">拉姆达查询语句如(it=>id.Id>1)</param>
        /// <returns>返回List集合</returns>
        public List<T> GetPageList(Expression<Func<T, bool>> whereLambda, int pageIndex = 1)
        {
            return GetPageList(whereLambda, pageIndex, orderField, Pages);
        }

        public List<T> GetPageList(Expression<Func<T, bool>> whereLambda, int pageIndex, string orderField)
        {
            return GetPageList(whereLambda, pageIndex, orderField, Pages);
        }
        public List<T> GetPageList(Expression<Func<T, bool>> whereLambda, int pageIndex, string orderField, PageModel page)
        {
            return CurrentDal.GetPageList(whereLambda, pageIndex, orderField, page);
        }
        #endregion

        #region 复杂的排序查询
        /// <summary>
        /// 查询返回全部ISugarQueryable对象
        /// </summary>
        /// <returns>ISugarQueryable</returns>
        public ISugarQueryable<T> GetDatas()
        {
            var getAll = CurrentDal.GetDatas();
            return getAll;
        }
        /// <summary>
        /// 自定义条件查询返回ISugarQueryable对象
        /// </summary>
        /// <param name="whereLambda">拉姆达查询语句如(it=>id.Id>1)</param>
        /// <returns>ISugarQueryable</returns>
        public ISugarQueryable<T> GetDatas(Expression<Func<T, bool>> whereLambda)
        {
            return CurrentDal.GetDatas(whereLambda);
        }


        public ISugarQueryable<T> GetPageDatas(int pageIndex = 1)
        {
            return GetPageDatas(pageIndex);
        }
        public ISugarQueryable<T> GetPageDatas(int pageIndex,string orderField)
        {
            return GetPageDatas(pageIndex, orderField);
        }
        public ISugarQueryable<T> GetPageDatas(int pageIndex, string orderField, PageModel page)
        {
            return CurrentDal.GetPageDatas(pageIndex, orderField, page);
        }

        /// <summary>
        /// 自定义条件查询数据
        /// </summary>
        /// <param name="pageIndex">当前页码数</param>
        /// <param name="page">页码PageModel</param>
        /// <param name="whereLambda">拉姆达查询语句如(it=>id.Id>1)</param>
        /// <returns>返回List集合</returns>
        public ISugarQueryable<T> GetPageDatas(Expression<Func<T, bool>> whereLambda, int pageIndex = 1)
        {
            return GetPageDatas(whereLambda, pageIndex, orderField, Pages);
        }
        public ISugarQueryable<T> GetPageDatas(Expression<Func<T, bool>> whereLambda, int pageIndex, string orderField)
        {
            return GetPageDatas(whereLambda, pageIndex, orderField, Pages);
        }
        public ISugarQueryable<T> GetPageDatas(Expression<Func<T, bool>> whereLambda, int pageIndex, string orderField, PageModel page)
        {
            return CurrentDal.GetPageDatas(whereLambda, pageIndex, orderField, page);
        }
        #endregion

        #region 查询是否存在数据 IsAny
        public bool IsAny(Expression<Func<T, bool>> whereLambda)
        {
            return CurrentDal.IsAny(whereLambda);
        }
        #endregion

        #endregion

        #region 插入
        /// <summary>
        /// 返回受影响的行数
        /// </summary>
        /// <param name="inObject">List,Dictionary,T对象,T[]数组</param>
        /// <returns></returns>
        public IInsertable<T> Insert(dynamic inObject)
        {
            return CurrentDal.Insert(inObject);
        }
        public IInsertable<T> Insert(dynamic inObject, Expression<Func<T, T>> fieldLambda)
        {
            return CurrentDal.Insert(inObject,fieldLambda);
        }
        /// <summary>
        /// 返回布尔值,更新操作 主键必须有值
        /// </summary>
        /// <param name="inObject">List,Dictionary,T对象,T[]数组</param>
        /// <returns></returns>
        public bool Add(dynamic inObject)
        {
            return CurrentDal.Add(inObject);
        }
        #endregion

        #region 修改
        /// <summary>
        /// 返回布尔值,更新操作 主键必须有值
        /// </summary>
        /// <param name="inObject">List,Dictionary,T对象,T[]数组</param>
        /// <returns></returns>
        public bool Save(dynamic updateObj)
        {
            return CurrentDal.Save(updateObj);
        }

        /// <summary>
        /// 返回布尔值,更新操作 主键必须有值
        /// </summary>
        /// <param name="updateObj"></param>
        /// <param name="field">true 为空得字段不更新</param>
        /// <returns></returns>
        public bool Save(T updateObj,bool field)
        {
            return CurrentDal.Save(updateObj,field);
        }

        /// <summary>
        /// 主键必须有值
        /// </summary>
        /// <param name="updateObj">List,Dictionary,T对象,T[]数组</param>
        /// <returns></returns>
        public IUpdateable<T> Update(dynamic updateObj)
        {
            return CurrentDal.Update(updateObj);
        }

        /// <summary>
        /// 常用更新的模式
        /// </summary>
        /// <param name="columns">需要更新的拉姆达表达式(如:it => new Student() { Name = "a", CreateTime = DateTime.Now })</param>
        /// <param name="whereExpression">拉姆达表达式(如:it => it.Id == 11)按照该条件更新</param>
        /// <returns></returns>
        public bool Update(Expression<Func<T, bool>> columns, Expression<Func<T, bool>> whereLambda)
        {
            return CurrentDal.Update(columns,whereLambda);
        }
        #endregion

        #region 删除
        public bool Delete(T deleteObj)
        {
            return CurrentDal.Delete(deleteObj);
        }
        public bool Delete(Expression<Func<T, bool>> whereLambda)
        {
            return CurrentDal.Delete(whereLambda);
        }
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="id">单个主键或数组</param>
        /// <returns>布尔值</returns>
        public bool Del(dynamic id)
        {
            return CurrentDal.Del(id);
        }
        /*public bool Del(dynamic[] ids)
        {
            return CurrentDal.Del(ids);
        }*/
        #endregion

        #region 聚合查询
        /// <summary>
        /// 返回数据总数
        /// </summary>
        /// <returns>整数</returns>
        public int Count()
        {
            return CurrentDal.Count();
        }
        /// <summary>
        /// 返回数据总数
        /// </summary>
        /// <param name="whereLambda">拉姆达表达式where</param>
        /// <returns>整数</returns>
        public int Count(Expression<Func<T, bool>> whereLambda)
        {
            return CurrentDal.Count(whereLambda);
        }
        #endregion

        public IAdo Ado()
        {
            return CurrentDal.Ado();
        }

        public ISugarQueryable<T> Search(ISugarQueryable<T> u, Dictionary<string, string> dic) {
            return CurrentDal.Search(u, dic);
        }
        public List<T> Search(ISugarQueryable<T> u, int pageIndex, string orderField, PageModel page) {
            return CurrentDal.Search(u, pageIndex, orderField, page);
        }


    }
}
