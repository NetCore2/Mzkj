﻿using Mzkj.IBLL;
using Mzkj.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mzkj.BLL
{
    public class UserRoleService : BaseService<UserRole>, IUserRoleService
    {
        public override void SetCurrentDal()
        {
            CurrentDal = DbSession.UserRoleDal; 
        }
    }
}
