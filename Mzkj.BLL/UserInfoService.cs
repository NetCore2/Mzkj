﻿using Mzkj.IBLL;
using Mzkj.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mzkj.BLL
{
    public class UserInfoService : BaseService<UserInfo>,IUserInfoService
    {
        /// <summary>
        /// 添加user和userInfo表的数据
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public override void SetCurrentDal()
        {
            CurrentDal=DbSession.UserInfoDal;
        }
    }
}
