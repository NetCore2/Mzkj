﻿using Mzkj.IBLL;
using Mzkj.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mzkj.BLL
{
    public class RoleService : BaseService<Role>, IRoleService
    {
        /// <summary>
        /// 设置权限
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public bool SetAction(int roleId, List<RoleAction> actions)
        {
            RoleActionService roleAction = new RoleActionService();

            bool b = roleAction.Delete(r => r.Rid == roleId);
            if (actions.Count > 0)
            {
                b = roleAction.Insert(actions.ToArray()).ExecuteCommand() > 0;
            }

            return b;
        }

        public override void SetCurrentDal()
        {
            CurrentDal = DbSession.RoleDal;
        }
    }
}
