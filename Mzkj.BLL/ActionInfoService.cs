﻿using Mzkj.IBLL;
using Mzkj.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mzkj.BLL
{
    public class ActionInfoService : BaseService<ActionInfo>,IActionInfoService
    {
        public override void SetCurrentDal()
        {
            CurrentDal = DbSession.ActionInfoDal;
        }
    }
}
