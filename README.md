# 权限管理系统
权限系统框架
边学边做的Asp.net权限系统框架 底层框架已完成(包括服务,工厂,抽象等)

1.使用Asp.net Core2.0 Mvc模式 搭建管理系统(正在进行中) 登录暂时用的两个模式是(选择使用) 

(1)Session 

(2)Cookie+Redis 

暂时不考虑官方的cookie认证方式.对我个人来说,控制不方便(主要还是自己才学asp.net吧.搞不动注入底层这些.都是边百度变做)

2.使用Asp.net Core2.0 Api 搭建后端分离的后台会用 React或者Vue +JWT (完成)

  注:(有其他项目,没使用React或者Vue,使用的layuicms2.0 感谢作者)

感谢:

    [layuicms2.0](https://gitee.com/layuicms/layuicms2.0)

    [layui](https://gitee.com/sentsin/layui)
    